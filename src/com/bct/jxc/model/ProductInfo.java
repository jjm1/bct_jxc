package com.bct.jxc.model;

import com.bct.jxc.common.StringUtil;

public class ProductInfo extends Product{
	private String providerName;// 多表联查获取供应商全称

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	/**
	 * 商品列表展示
	 */
	@Override
	public Object getValue(Integer colmnNumber) {
		switch (colmnNumber) {
		case 0://编号
			return StringUtil.changeNull(getId());
		case 1://名字
			return StringUtil.changeNull(getProductName());
		case 2://简称
			return StringUtil.changeNull(getShorts());
		case 3://产地
			return StringUtil.changeNull(getPlace());
		case 4://规格
			return StringUtil.changeNull(getSpecification());
		case 5://包装
			return StringUtil.changeNull(getPacking());
		case 6://批号
			return StringUtil.changeNull(getBatch());
		case 7://批准文号
			return StringUtil.changeNull(getApprovalNumber());
		case 8://价格
			return StringUtil.changeNull(getPrice());
		case 9://备注
			return StringUtil.changeNull(getRemark());
		case 10://供应商全称?
			return StringUtil.changeNull(getProviderName());
		default:
			return "";
		}
	}
}
