package com.bct.jxc.model;

public abstract class BaseModel {
	/**
	 * 表格控件调用
	 * @param colmnNumber:对应的表字段
	 * @return
	 */
	public abstract Object getValue(Integer colmnNumber);
}
