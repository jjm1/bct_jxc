package com.bct.jxc.model;

import com.bct.jxc.common.StringUtil;

/**
 * 提取供应商和客户的公共信息
 * @author ASUS
 *
 */
public class Person extends BaseModel{
	private String id;//编号
	private String customerName;//客户全称
	private String shorts;//简称
	private String address;//客户地址
	private String zip;//客户邮编
	private String telephone;//客户电话
	private String fax;//客户传真
	private String contracts;//联系人
	private String contractsTele;//联系人电话
	private String bank;//开户银行
	private String account;//银行账号
	private String email;//邮箱
	private int spare;//是否可用：1，可用  0，不可用
	
	public Person() {
		super();
	}
	
	public Person(String id, String customerName, String shorts, String address, String zip, String telephone,
			String fax, String contracts, String contractsTele, String bank, String account, String email,
			int spare) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.shorts = shorts;
		this.address = address;
		this.zip = zip;
		this.telephone = telephone;
		this.fax = fax;
		this.contracts = contracts;
		this.contractsTele = contractsTele;
		this.bank = bank;
		this.account = account;
		this.email = email;
		this.spare = spare;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShorts() {
		return shorts;
	}
	public void setShorts(String shorts) {
		this.shorts = shorts;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getContracts() {
		return contracts;
	}
	public void setContracts(String contracts) {
		this.contracts = contracts;
	}
	public String getContractsTele() {
		return contractsTele;
	}
	public void setContractsTele(String contractsTele) {
		this.contractsTele = contractsTele;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getSpare() {
		return spare;
	}
	public void setSpare(int spare) {
		this.spare = spare;
	}
	/**
	 * 表格控件调用
	 * @param colmnNumber
	 * @return
	 */
	public Object getValue(Integer colmnNumber) {
		switch (colmnNumber) {
		case 0://编号
			return StringUtil.changeNull(getId());
		case 1://名字
			return StringUtil.changeNull(getCustomerName());
		case 2://简称
			return StringUtil.changeNull(getShorts());
		case 3://客户地址
			return StringUtil.changeNull(getAddress());
		case 4://邮编
			return StringUtil.changeNull(getZip());
		case 5://客户电话
			return StringUtil.changeNull(getTelephone());
		case 6://传真
			return StringUtil.changeNull(getFax());
		case 7://联系人
			return StringUtil.changeNull(getContracts());
		case 8://联系人电话
			return StringUtil.changeNull(getContractsTele());
		case 9://银行
			return StringUtil.changeNull(getBank());
		case 10://银行账号
			return StringUtil.changeNull(getAccount());
		case 11://邮箱
			return StringUtil.changeNull(getEmail());
		default:
			return "";
		}
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", customerName=" + customerName + ", shorts=" + shorts + ", address=" + address
				+ ", zip=" + zip + ", telephone=" + telephone + ", fax=" + fax + ", contracts=" + contracts
				+ ", contractsTele=" + contractsTele + ", bank=" + bank + ", account=" + account + ", email=" + email
				+ ", spare=" + spare + "]";
	}
	
}
