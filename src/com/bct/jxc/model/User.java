package com.bct.jxc.model;

/**
 * 用户实体
 * @author ASUS
 *
 */
public class User {
	private String loginName;//登录名
	private String password;//密码
	private String username;//用户名
	private String power;//权限
	
	public User() {
		super();
	}
	public User(String loginName, String password, String username, String power) {
		super();
		this.loginName = loginName;
		this.password = password;
		this.username = username;
		this.power = power;
	}
	public User(String loginName, String password) {
		super();
		this.loginName = loginName;
		this.password = password;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	@Override
	public String toString() {
		return "User [loginName=" + loginName + ", password=" + password + ", username=" + username + ", power=" + power
				+ "]";
	}
	
	
}
