package com.bct.jxc.model;

import com.bct.jxc.common.StringUtil;

/**
 * 商品实体类，对应t_product表
 * @author ASUS
 *
 */
public class Product extends BaseModel{
	private String id;//编号
	private String productName;//商品名称
	private String shorts;//简称
	private String place;//产地
	private String specification;//规格
	private String packing;//包装
	private String batch;//批号
	private String approvalNumber;//批准文号
	private float price;//价格
	private String remark;//备注
	private String providerId;//供应商编号
	private int spare;//备用字段
	public Product() {
		super();
	}
	public Product(String id, String productName, String shorts, String place, String specification, String packing,
			String batch, String approvalNumber, float price, String remark, String providerId, int spare) {
		super();
		this.id = id;
		this.productName = productName;
		this.shorts = shorts;
		this.place = place;
		this.specification = specification;
		this.packing = packing;
		this.batch = batch;
		this.approvalNumber = approvalNumber;
		this.price = price;
		this.remark = remark;
		this.providerId = providerId;
		this.spare = spare;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShorts() {
		return shorts;
	}
	public void setShorts(String shorts) {
		this.shorts = shorts;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getPacking() {
		return packing;
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getApprovalNumber() {
		return approvalNumber;
	}
	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public int getSpare() {
		return spare;
	}
	public void setSpare(int spare) {
		this.spare = spare;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", shorts=" + shorts + ", place=" + place
				+ ", specification=" + specification + ", packing=" + packing + ", batch=" + batch + ", approvalNumber="
				+ approvalNumber + ", price=" + price + ", remark=" + remark + ", providerId=" + providerId
				+ ", spare=" + spare + "]";
	}
	@Override
	public Object getValue(Integer colmnNumber) {
		return null;
	}
}
