package com.bct.jxc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bct.jxc.common.Constants;
import com.bct.jxc.common.ErrorManager;
import com.bct.jxc.common.StringUtil;
import com.bct.jxc.model.Customer;

/**
 * 客户类Dao
 * 针对(t_user表进行CRUD)
 * @author ASUS
 *
 */
public class CustomerDao implements BaseDao<Customer>{
	SqlManager manager = null;
	
	public CustomerDao() {
		super();
		manager = SqlManager.createInstance();
		manager.connectDB();//换取数据库连接
	}
	/**
	 * 添加客户信息
	 * @param customer
	 * @return
	 */
	@Override
	public boolean add(Customer customer) {
		String sql = "insert into t_customer values(?,?,?,?,?,?,?,?,?,?,?,?,1)";//sql语句
		//封装参数
		Object[] params = {customer.getId(),customer.getCustomerName(),customer.getShorts(),customer.getAddress(),
				customer.getZip(),customer.getTelephone(),customer.getFax(),customer.getContracts(),
				customer.getContractsTele(),customer.getBank(),customer.getAccount(),customer.getEmail()};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);//更新数据库操作
	}
	/**
	 * 生成客户编号
	 * @return
	 */
	@Override
	public String getId() {
		//sql语句，获得最新的客户编号
		String sql = "select MAX(id) as id from t_customer";
		ResultSet rs = manager.executeQuery(sql, null, Constants.PSTM_TYPE);
		String id = "CT1001";
		try {
			if(rs != null && rs.next()) {
				String sid = rs.getString("id");// 获取前一次插入的id
				if(sid != null) {
					String s = sid.substring(2);
					id = "CT" + (Integer.parseInt(s) + 1);
				}
			}
		} catch (SQLException e) {
			ErrorManager.printError("CustomerDao getCustomerId()", e);
		}
		return id;
	}
	/**
	 * 查询客户信息
	 * @param c
	 * @return
	 */
	@Override
	public List<Customer> finds(Customer c){
		List<Customer> list = null;
		String sql = null;
		Object[] params = null;
		if(c == null) {
			sql = "select * from t_customer where spare = 1";
		}else {
			//根据ID查询
			if(!StringUtil.isEmpty(c.getId())) {
				sql = "select * from t_customer where id=? and spare = 1";
				params = new Object[] {c.getId()};
			}else if(!StringUtil.isEmpty(c.getCustomerName())) {//根据客户全称查询
				sql = "select * from t_customer where customer_name like ? and spare = 1";
				params = new Object[] {"%"+c.getCustomerName()+"%"};
			}else if(!StringUtil.isEmpty(c.getShorts())) {//根据客户简称查询
				sql = "select * from t_customer where shorts like ? and spare = 1";
				params = new Object[] {"%"+c.getShorts()+"%"};
			}else if(!StringUtil.isEmpty(c.getContracts())) {//根据联系人查询
				sql = "select * from t_customer where contracts like ? and spare = 1";
				params = new Object[] {"%"+c.getContracts()+"%"};
			}
		}
		ResultSet rs = manager.executeQuery(sql, params, Constants.PSTM_TYPE);
		if(rs != null) {
			list = new ArrayList<Customer>();
			try {
				while(rs.next()) {
					String id = rs.getString("id");
					String customerName = rs.getString("customer_name");
					String shorts = rs.getString("shorts");
					String address = rs.getString("address");
					String zip = rs.getString("zip");
					String telephone = rs.getString("telephone");
					String fax = rs.getString("fax");
					String contracts = rs.getString("contracts");
					String contractsTele = rs.getString("contracts_tele");
					String bank = rs.getString("bank");
					String account = rs.getString("account");
					String email = rs.getString("email");
					int spare = Integer.parseInt(rs.getString("spare"));
					Customer customer = new Customer();
					customer.setId(id);
					customer.setCustomerName(customerName);
					customer.setShorts(shorts);
					customer.setAddress(address);
					customer.setZip(zip);
					customer.setTelephone(telephone);
					customer.setFax(fax);
					customer.setContracts(contracts);
					customer.setContractsTele(contractsTele);
					customer.setBank(bank);
					customer.setAccount(account);
					customer.setEmail(email);
					customer.setSpare(spare);
					list.add(customer);
				}
			} catch (SQLException e) {
				ErrorManager.printError("CustomerDao findCustomers(Customer c)", e);
			}
		}
		return list;
	}
	/**
	 * 根据客户编号修改客户信息
	 * @param customer
	 * @return
	 */
	@Override
	public boolean update(Customer customer) {
		//编写sql语句
		String sql = "update t_customer set customer_name=?,shorts=?,address=?,zip=?,"
				+ "telephone=?,fax=?,contracts=?,contracts_tele=?,bank=?,account=?,"
				+ "email=? where id=?";
		//封装参数
		Object params[] = {customer.getCustomerName(),customer.getShorts(),customer.getAddress(),
				customer.getZip(),customer.getTelephone(),customer.getFax(),customer.getContracts(),
				customer.getContractsTele(),customer.getBank(),customer.getAccount(),customer.getEmail(),
				customer.getId()};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);
	}
	/**
	 * 根据客户编号修改客户spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	@Override
	public boolean update(String id,Integer spare) {
		//编写sql语句
		String sql = "update t_customer set spare=? where id=?";
		//封装参数
		Object params[] = {spare!=null?spare:1,id};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);
	}
}

