package com.bct.jxc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bct.jxc.common.Constants;
import com.bct.jxc.common.ErrorManager;
import com.bct.jxc.common.StringUtil;
import com.bct.jxc.model.Customer;
import com.bct.jxc.model.Provider;

/**
 * 供应商类Dao
 * 针对(t_provider表进行CRUD)
 * @author ASUS
 */
public class ProviderDao implements BaseDao<Provider>{
	SqlManager manager = null;
	
	public ProviderDao() {
		super();
		manager = SqlManager.createInstance();
		manager.connectDB();//获取数据库连接
	}
	/**
	 * 添加供应商信息
	 * @param provider
	 * @return
	 */
	@Override
	public boolean add(Provider provider) {
		//编写sql语句
		String sql = "insert into t_provider values(?,?,?,?,?,?,?,?,?,?,?,?,1)";
		//封装参数
		Object[] params = {provider.getId(),provider.getCustomerName(),provider.getShorts(),provider.getAddress(),
						provider.getZip(),provider.getTelephone(),provider.getFax(),provider.getContracts(),
						provider.getContractsTele(),provider.getBank(),provider.getAccount(),provider.getEmail()};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);
	}
	/**
	 * 生成供应商编号
	 * @return
	 */
	@Override
	public String getId() {
		//sql语句，获得最新的供应商编号
		String sql = "select MAX(id) as id from t_provider";
		ResultSet rs = manager.executeQuery(sql, null, Constants.PSTM_TYPE);
		String id = "PR1001";
		try {
			if(rs != null && rs.next()) {
				String sid = rs.getString("id");// 获取前一次插入的id
				if(sid != null) {
					String s = sid.substring(2);
					id = "PR" + (Integer.parseInt(s) + 1);
				}
			}
		} catch (SQLException e) {
			ErrorManager.printError("ProviderDao ggetProviderId()", e);
		}
		return id;
	}
	/**
	 * 根据供应商编号修改供应商信息
	 * @param provider
	 * @return
	 */
	@Override
	public boolean update(Provider provider) {
		//编写sql语句
		String sql = "update t_provider set provider_name=?,shorts=?,address=?,zip=?,"
				+ "telephone=?,fax=?,contacts=?,contacts_telephone=?,bank=?,account=?,"
				+ "email=? where id=?";
		//封装参数
		Object params[] = {provider.getCustomerName(),provider.getShorts(),provider.getAddress(),
				provider.getZip(),provider.getTelephone(),provider.getFax(),provider.getContracts(),
				provider.getContractsTele(),provider.getBank(),provider.getAccount(),provider.getEmail(),
				provider.getId()};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);
	}
	/**
	 * 根据供应商编号修改供应商spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	@Override
	public boolean update(String id,Integer spare) {
		//编写sql语句
		String sql = "update t_provider set spare=? where id=?";
		//封装参数
		Object params[] = {spare!=null?spare:1,id};
		return manager.executeUpdate(sql, params, Constants.PSTM_TYPE);
	}
	/**
	 * 查询商品信息
	 */
	@Override
	public List<Provider> finds(Provider provider) {
		List<Provider> list = null;
		String sql = null;
		Object[] params = null;
		if(provider == null) {
			sql = "select * from t_provider where spare = 1";
		}else {
			//根据ID查询
			if(!StringUtil.isEmpty(provider.getId())) {
				sql = "select * from t_provider where id=? and spare = 1";
				params = new Object[] {provider.getId()};
			}else if(!StringUtil.isEmpty(provider.getCustomerName())) {//根据客户全称查询
				sql = "select * from t_provider where provider_name like ? and spare = 1";
				params = new Object[] {"%"+provider.getCustomerName()+"%"};
			}else if(!StringUtil.isEmpty(provider.getShorts())) {//根据客户简称查询
				sql = "select * from t_provider where shorts like ? and spare = 1";
				params = new Object[] {"%"+provider.getShorts()+"%"};
			}else if(!StringUtil.isEmpty(provider.getContracts())) {//根据联系人查询
				sql = "select * from t_provider where contacts like ? and spare = 1";
				params = new Object[] {"%"+provider.getContracts()+"%"};
			}
		}
		ResultSet rs = manager.executeQuery(sql, params, Constants.PSTM_TYPE);
		if(rs != null) {
			list = new ArrayList<Provider>();
			try {
				while(rs.next()) {
					String id = rs.getString("id");
					String customerName = rs.getString("provider_name");
					String shorts = rs.getString("shorts");
					String address = rs.getString("address");
					String zip = rs.getString("zip");
					String telephone = rs.getString("telephone");
					String fax = rs.getString("fax");
					String contracts = rs.getString("contacts");
					String contractsTele = rs.getString("contacts_telephone");
					String bank = rs.getString("bank");
					String account = rs.getString("account");
					String email = rs.getString("email");
					int spare = Integer.parseInt(rs.getString("spare"));
					Provider p = new Provider();
					p.setId(id);
					p.setCustomerName(customerName);
					p.setShorts(shorts);
					p.setAddress(address);
					p.setZip(zip);
					p.setTelephone(telephone);
					p.setFax(fax);
					p.setContracts(contracts);
					p.setContractsTele(contractsTele);
					p.setBank(bank);
					p.setAccount(account);
					p.setEmail(email);
					p.setSpare(spare);
					list.add(p);
				}
			} catch (SQLException e) {
				ErrorManager.printError("ProviderDao finds(Customer c)", e);
			}
		}
		return list;
	}
}
