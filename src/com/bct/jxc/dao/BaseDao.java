package com.bct.jxc.dao;

import java.util.List;

import com.bct.jxc.model.Customer;

/**
 * 提取信息的公共接口
 * @author ASUS
 *
 */
public interface BaseDao<T> {
	/**
	 * 添加信息
	 * @param t
	 * @return
	 */
	public boolean add(T t);
	/**
	 * 生成编号
	 * @return
	 */
	public String getId();
	/**
	 * 查询信息
	 * @param t
	 * @return
	 */
	public List<T> finds(T t);
	/**
	 * 根据编号修改信息
	 * @param t
	 * @return
	 */
	public boolean update(T t);
	/**
	 * 根据编号修改spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	public boolean update(String id,Integer spare);
}
