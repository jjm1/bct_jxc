package com.bct.jxc.text;

import org.junit.Before;
import org.junit.Test;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.model.User;
import com.bct.jxc.server.UserServer;

/**
 * 测试类
 * @author ASUS
 *
 */
public class UserTest {
	/**
	 * 根据用户名和密码查询
	 */
	@Test
	public void testLoginCheck() {
		UserServer userServer = CommonFactory.getUserServer();
		boolean res = userServer.loginCheck(new User("admin","123456"));
		if(res == true) {
			System.out.println("登录成功！");
		}else {
			System.out.println("登录失败！");
		}
	}
	
	@Test
	public void testGet() {
		UserServer userServer = CommonFactory.getUserServer();
		User user = userServer.getUser("admin");
		System.out.println(user);
	}
}
