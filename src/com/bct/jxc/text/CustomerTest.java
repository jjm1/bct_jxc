package com.bct.jxc.text;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.Constants;
import com.bct.jxc.model.Customer;
import com.bct.jxc.server.CustomerServer;

/**
 * 客户测试
 * @author ASUS
 *
 */
public class CustomerTest {
	 CustomerServer service  = null;
	 /**
	  * 每次在执行@Test方法之前，都会执行一次
	  */
	 @Before
	 public void init() {
		  service = CommonFactory.getCustomerService();
	 }

	/**
	 * 添加客户信息
	 */
	@Test
	public void testAddCustomer() { 
	   Customer customer = new Customer("CT1002", "阿里巴巴有限公司", "阿里巴巴", "杭州阿里巴巴总部", "888888",
			   "123456789", "000-000-000", "马云", "13852015201", "招商银行", "42111111111", "mayun@520.com", 1);
	   boolean result = service.addCustomer(customer);
	   if(result==true) {
		   System.out.println("客户信息添加成功！");
	   }else {
		   System.out.println("客户信息添加失败！");
	   }
	}
	/**
	 * 动态生成客户id
	 */
	@Test
	public void testGetCustomerId() {
		String id = service.getCustomerId();
		System.out.println(id);
	}
	/**
	 * 查询客户信息
	 */
	@Test
	public void testFindCustomers(){
		Customer c = new Customer();
		//c.setId("CT1002");
		//c.setCustomerName("里");
		//c.setShorts("东");
		c.setContracts("刘");
		List<Customer> list = service.findCustomers(c);
		for (Customer customer : list) {
			System.out.println(customer);
		}
	}
	/**
	 * 根据客户编号，修改客户信息
	 */
	@Test
	public void testUpadateCustomer() {
		Customer customer = new Customer("CT1002", "阿里巴巴有限公司", "阿里巴巴", "杭州阿里巴巴总部", "345678",
				   "123456789", "000-000-000", "马云", "13852015201", "招商银行", "42111111111", 
				   "mayun@520.com", 1);
		boolean res = service.updateCustomer(customer);
		if(res) {
			System.out.println("修改客户信息成功！");
		}else {
			System.out.println("修改客户信息失败！");
		}
	}
	/**
	 * 根据客户编号修改客户spare
	 */
	@Test
	public void testUpdateSpare() {
		boolean res = service.updateCustomer("CT1004", Constants.DATA_FIND);
		if(res) {
			System.out.println("修改客户spare成功！");
		}else {
			System.out.println("修改客户spare失败！");
		}
	}
}
