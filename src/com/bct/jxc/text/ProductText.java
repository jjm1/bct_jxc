package com.bct.jxc.text;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.Constants;
import com.bct.jxc.model.Customer;
import com.bct.jxc.model.Product;
import com.bct.jxc.model.Provider;
import com.bct.jxc.server.ProductServer;

/**
 * 商品测试
 * 
 * @author ASUS
 *
 */
public class ProductText {
	ProductServer server = null;

	/**
	 * 每次在执行@Test方法之前，都会执行一次
	 */
	@Before
	public void init() {
		server = CommonFactory.getProductServer();
	}

	/**
	 * 添加商品信息
	 */
	@Test
	public void testAddProduct() {
		Product product = new Product("PD1002","华为P30", "华为", "北京", "124456789", "盒子ww",
				"2345678", "13852015201", 2999, "新品测试", "PR1002", 1);
		boolean result = server.addProduct(product);
		if (result) {
			System.out.println("商品添加成功！");
		} else {
			System.out.println("商品添加失败！");
		}
	}
	/**
	 * 动态生成商品id
	 */
	@Test
	public void testGetProductId() {
		String id = server.getProductId();
		System.out.println(id);
	}
	/**
	 * 根据商品编号，修改商品信息
	 */
	@Test
	public void testUpadateProduct() {
		Product product = new Product("PD1001", "小米9手机", "小米", "上海", "888898", "包装纸", "2345678", "13852015201",
				2440, "降价售卖", "PR1001",1);
		boolean res = server.updateProduct(product);
		if (res) {
			System.out.println("修改商品信息成功！");
		} else {
			System.out.println("修改商品信息失败！");
		}
	}
	/**
	 * 根据商品编号修改客户spare
	 */
	@Test
	public void testUpdateSpare() {
		boolean res = server.updateProduct("PD1001", Constants.DATA_FIND);
		if (res) {
			System.out.println("修改商品spare成功！");
		} else {
			System.out.println("修改商品spare失败！");
		}
	}
	/**
	 * 查询客户信息
	 */
	@Test
	public void testFindCustomers(){
		Product p = new Product();
		p.setId("PD1001");
		List<Product> list = server.findProducts(p);
		for (Product product : list) {
			System.out.println(product);
		}
	}
}
