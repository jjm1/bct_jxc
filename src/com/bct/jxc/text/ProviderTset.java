package com.bct.jxc.text;
/**
 * 供应商测试
 * @author ASUS
 *
 */

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.Constants;
import com.bct.jxc.model.Customer;
import com.bct.jxc.model.Provider;
import com.bct.jxc.server.ProviderServer;

public class ProviderTset {
	ProviderServer server = null;

	/**
	 * 每次在执行@Test方法之前，都会执行一次
	 */
	@Before
	public void init() {
		server = CommonFactory.getProviderServer();
	}

	/**
	 * 添加供应商信息
	 */
	@Test
	public void testAddCustomer() {
		Provider provider = new Provider("PR1003", "加工厂", "虎成", "杭州", "885898", "123454589", "050-666-005", "老胡",
				"13852012456", "建设银行", "421634578111", "mayun@520.com", 1);
		boolean result = server.addProvider(provider);
		if (result) {
			System.out.println("供应商信息添加成功！");
		} else {
			System.out.println("供应商信息添加失败！");
		}
	}

	/**
	 * 动态生成供应商id
	 */
	@Test
	public void testGetProviderId() {
		String id = server.getProviderId();
		System.out.println(id);
	}

	/**
	 * 根据供应商编号，修改供应商信息
	 */
	@Test
	public void testUpadateProvider() {
		Provider provider = new Provider("PR1003", "加工厂", "虎成", "杭州", "885898", "123454589", "050-666-005", "老胡",
				"13852012456", "建设银行", "421634578111", "mayun@520.com", 1);
		boolean res = server.updateProvider(provider);
		if (res) {
			System.out.println("修改供应商信息成功！");
		} else {
			System.out.println("修改供应商信息失败！");
		}
	}

	/**
	 * 根据供应商编号修改客户spare
	 */
	@Test
	public void testUpdateSpare() {
		boolean res = server.updateProvider("PR1001", Constants.DATA_FIND);
		if (res) {
			System.out.println("修改供应商spare成功！");
		} else {
			System.out.println("修改供应商spare失败！");
		}
	}
	/**
	 * 查询商品信息
	 */
	@Test
	public void testFindProviders(){
		//Provider provider = new Provider();
		List<Provider> list = server.finds(null);
		for (Provider p : list) {
			System.out.println(p);
		}
	}
}
