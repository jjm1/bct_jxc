package com.bct.jxc.server;

import java.util.List;

import com.bct.jxc.model.Product;
import com.bct.jxc.model.ProductInfo;

public interface ProductServer {
	/**
	 * 添加商品信息
	 * @param product
	 * @return
	 */
	public boolean addProduct(Product product);
	/**
	 * 生成商品编号
	 * @return
	 */
	public String getProductId();
	/**
	 * 根据商品编号修改供应商信息
	 * @param provider
	 * @return
	 */
	public boolean updateProduct(Product product);
	/**
	 * 根据商品编号修改商品spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	public boolean updateProduct(String id,Integer spare);
	/**
	 * 查询商品信息
	 * @param product
	 * @return
	 */
	public List<Product> findProducts(Product product);
	/**
	 * 查询商品
	 */
	public List<ProductInfo> findProducts(ProductInfo p); 
}
