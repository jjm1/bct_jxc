package com.bct.jxc.server;

import java.util.List;

import com.bct.jxc.model.Customer;

/**
 * 客户业务接口
 * @author ASUS
 *
 */
public interface CustomerServer {
	/**
	 * 添加客户信息
	 * @param customer
	 * @return
	 */
	public boolean addCustomer(Customer customer);
	/**
	 * 生成客户编号
	 * @return
	 */
	public String getCustomerId();
	/**
	 * 查询客户信息
	 * @param c
	 * @return
	 */
	public List<Customer> findCustomers(Customer c);
	/**
	 * 根据客户编号修改客户信息
	 * @param customer
	 * @return
	 */
	public boolean updateCustomer(Customer customer);
	/**
	 * 根据客户编号修改客户spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	public boolean updateCustomer(String id,Integer spare);
}
