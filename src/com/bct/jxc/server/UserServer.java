package com.bct.jxc.server;

import com.bct.jxc.model.Customer;
import com.bct.jxc.model.User;

/**
 * 操作员业务接口
 * @author ASUS
 *
 */
public interface UserServer {
	/**
	 * 查询用户账号密码是否正确
	 * @param user
	 * @return
	 */
	boolean loginCheck(User user);
	/**
	 * 通过登录名查询操作者信息
	 * @param username
	 * @return
	 */
	public User getUser(String username);
	
}
