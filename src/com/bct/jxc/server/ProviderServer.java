package com.bct.jxc.server;

import java.util.List;

import com.bct.jxc.model.Provider;

public interface ProviderServer {
	/**
	 * 添加供应商信息
	 * @param provider
	 * @return
	 */
	public boolean addProvider(Provider provider);
	/**
	 * 生成供应商编号
	 */
	public String getProviderId();
	/**
	 * 根据客户编号修改供应商信息
	 * @param provider
	 * @return
	 */
	public boolean updateProvider(Provider provider);
	/**
	 * 根据供应商编号修改客户spare
	 * @param id
	 * @param spare 0表示删除，1表示找回
	 * @return
	 */
	public boolean updateProvider(String id,Integer spare);
	/**
	 * 查询商品信息
	 */
	public List<Provider> finds(Provider provider);
}
