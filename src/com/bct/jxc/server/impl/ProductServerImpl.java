package com.bct.jxc.server.impl;

import java.util.List;

import com.bct.jxc.dao.ProductDao;
import com.bct.jxc.model.Product;
import com.bct.jxc.model.ProductInfo;
import com.bct.jxc.server.ProductServer;

public class ProductServerImpl implements ProductServer{
	ProductDao productDao = new ProductDao();
	
	@Override
	public boolean addProduct(Product product) {
		return productDao.add(product);
	}

	@Override
	public String getProductId() {
		return productDao.getId();
	}

	@Override
	public boolean updateProduct(Product product) {
		return productDao.update(product);
	}

	@Override
	public boolean updateProduct(String id, Integer spare) {
		return productDao.update(id, spare);
	}

	@Override
	public List<Product> findProducts(Product product) {
		return productDao.finds(product);
	}

	@Override
	public List<ProductInfo> findProducts(ProductInfo p) {
		return productDao.finds(p);
	}

}
