package com.bct.jxc.server.impl;

import java.util.List;

import com.bct.jxc.dao.ProviderDao;
import com.bct.jxc.model.Provider;
import com.bct.jxc.server.ProviderServer;

public class PoviderServerImpl implements ProviderServer{
	ProviderDao providerDao = new ProviderDao();
	
	@Override
	public boolean addProvider(Provider provider) {
		return providerDao.add(provider);
	}

	@Override
	public String getProviderId() {
		return providerDao.getId();
	}

	@Override
	public boolean updateProvider(Provider provider) {
		return providerDao.update(provider);
	}

	@Override
	public boolean updateProvider(String id, Integer spare) {
		return providerDao.update(id, spare);
	}

	@Override
	public List<Provider> finds(Provider provider) {
		return providerDao.finds(provider);
	}

}
