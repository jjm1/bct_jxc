package com.bct.jxc.server.impl;

import java.util.List;

import com.bct.jxc.dao.CustomerDao;
import com.bct.jxc.model.Customer;
import com.bct.jxc.server.CustomerServer;

public class CustomerServerImpl implements CustomerServer{
	
	CustomerDao customerDao = new CustomerDao();
	@Override
	public boolean addCustomer(Customer customer) {
		return customerDao.add(customer);
	}
	@Override
	public String getCustomerId() {
		return customerDao.getId();
	}
	@Override
	public List<Customer> findCustomers(Customer c) {
		return customerDao.finds(c);
	}
	@Override
	public boolean updateCustomer(Customer customer) {
		return customerDao.update(customer);
	}
	@Override
	public boolean updateCustomer(String id, Integer spare) {
		return customerDao.update(id, spare);
	}

}
