package com.bct.jxc.server.impl;

import com.bct.jxc.dao.UserDao;
import com.bct.jxc.model.User;
import com.bct.jxc.server.UserServer;

public class UserServerImpl implements UserServer{
	private UserDao userDao = new UserDao();
	@Override
	public boolean loginCheck(User user) {
		return userDao.loginCheck(user);
	}
	/**
	 * 通过登录名查询操作者信息
	 */
	@Override
	public User getUser(String username) {
		return userDao.getUser(username);
	}

}
