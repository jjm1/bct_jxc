package com.bct.jxc;

import javax.swing.JFrame;

import com.bct.jxc.view.LoginFrame;

/**
 * 整个程序的入口类
 * @author ASUS
 *
 */
public class MainApplication {
	public static void main(String[] args) {
		//JFrame.setDefaultLookAndFeelDecorated(true);
		LoginFrame frame = new LoginFrame();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);//居中
	}
}
