package com.bct.jxc.common;

/**
 * 定义常量接口
 * @author ASUS
 *
 */
public class Constants {
	public static final int PSTM_TYPE = 0;//传统的预处理方式
	public static final int CALL_TYPE = 1;//访问存储过程
	public static final int DATA_DEL= 0;//表示删除数据
    public static final int DATA_FIND = 1;//表示找回数据
	
	//业务类的完整包名
	static final String USER_SERVICE_CLASS = "com.bct.jxc.server.impl.UserServerImpl";//操作者业务类
	static final String CUSTOMER_SERVICE_CLASS = "com.bct.jxc.server.impl.CustomerServerImpl";//客户务类
	static final String PROVIDER_SERVICE_CLASS = "com.bct.jxc.server.impl.PoviderServerImpl";//供应商务类
	static final String PRODUCT_SERVICE_CLASS = "com.bct.jxc.server.impl.ProductServerImpl";//商品务类
}
