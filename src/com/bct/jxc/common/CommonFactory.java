package com.bct.jxc.common;

import com.bct.jxc.server.CustomerServer;
import com.bct.jxc.server.ProductServer;
import com.bct.jxc.server.ProviderServer;
import com.bct.jxc.server.UserServer;

/**
 * 工厂类，产生各个业务类的对象
 * 
 * @author ASUS
 *
 */
public class CommonFactory {
	/**
	 * 获取UserServer类的实例
	 * 
	 * @return
	 */
	public static UserServer getUserServer() {
		try {
			return (UserServer) Class.forName(Constants.USER_SERVICE_CLASS).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			ErrorManager.printError("CommonFactory getUserServer()", e);
		}
		return null;
	}

	/**
	 * 获取CustomerService类的实例
	 * 
	 * @return
	 */
	public static CustomerServer getCustomerService() {
		try {
			return (CustomerServer) Class.forName(Constants.CUSTOMER_SERVICE_CLASS).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			ErrorManager.printError("CommonFactory getCustomerService()", e);
		}
		return null;
	}

	/**
	 * 获取ProviderServer类的实例
	 * 
	 * @return
	 */
	public static ProviderServer getProviderServer() {
		try {
			return (ProviderServer) Class.forName(Constants.PROVIDER_SERVICE_CLASS).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			ErrorManager.printError("CommonFactory getProviderServer()", e);
		}
		return null;
	}

	/**
	 * 获取ProductServer类的实例
	 * 
	 * @return
	 */
	public static ProductServer getProductServer() {
		try {
			return (ProductServer) Class.forName(Constants.PRODUCT_SERVICE_CLASS).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			ErrorManager.printError("CommonFactory getProductServer()", e);
		}
		return null;
	}
}
