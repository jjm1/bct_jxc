package com.bct.jxc.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 * @author ASUS
 *
 */
public class StringUtil {
	/**
	 * 判断是否为空
	 * @param s
	 * @return
	 */
	public static boolean isEmpty(String s) {
		if(s == null || "".equals(s.trim()) || s.length()==0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 处理null
	 * @param str 带处理的对象
	 * @return 处理结果
	 */
	public static Object changeNull(Object obj)
	{
		if(null==obj)
			return "";
		return obj;
	}
	/**
	 * 验证邮政编码是否合法
	 * @param s 被验证的邮编字符串
	 * @return 验证结果
	 */
	public static boolean isZip(String s) {
		String str = "^[1-9]\\d{5}$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(s);
		return m.matches();
	}
	/**
	 * 验证电话是否符合规则
	 * @param phone 被验证的电话字符串
	 * @return 验证结果
	 */
	public static boolean isPhone(String phone)
	{
		String eL= "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";		 
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(phone);    
        return m.matches();
	}
	
	/**
	 * 验证邮箱是否符合规则
	 * @param email 被验证的邮箱字符串
	 * @return 验证结果
	 */
	public static boolean isEmail(String email)
	{
		String eL= "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"; 
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(email);    
        return m.matches();
	}
	/**
	 * 验证价格是否符合规则
	 * @param email 被验证的价格字符串
	 * @return 验证结果
	 */
	public static boolean isPrice(String price)
	{
		String eL= "\\d+(.\\d+)?"; 
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(price);    
        boolean result = m.matches();   
        return result;
	}
	
	public static void main(String[] args) {
		System.out.println(isZip("123456"));
		System.out.println(isPhone("13852015201"));
		System.out.println(isEmail("mayun@520.com"));
	}
	
}
