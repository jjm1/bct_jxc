package com.bct.jxc.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.StringUtil;
import com.bct.jxc.model.User;
import com.bct.jxc.server.UserServer;

import java.awt.Toolkit;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginFrame extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField userName;//账号
	private JPasswordField password;//密码

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		//视窗锁定
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(LoginFrame.class.getResource("/com/bct/jxc/images/sm_jxc.png")));
		setTitle("用户登录");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//标题上的开关按钮
		setBounds(100, 100, 504, 341);//设置窗体大小
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("企业进销存管理系统");
		lblNewLabel.setIcon(new ImageIcon(LoginFrame.class.getResource("/com/bct/jxc/images/big_jxc.png")));
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 28));
		
		JLabel lblNewLabel_1 = new JLabel("账号");
		lblNewLabel_1.setIcon(new ImageIcon(LoginFrame.class.getResource("/com/bct/jxc/images/uname1.png")));
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 16));
		
		userName = new JTextField();
		userName.setFont(new Font("宋体", Font.PLAIN, 16));
		userName.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("密码");
		lblNewLabel_2.setIcon(new ImageIcon(LoginFrame.class.getResource("/com/bct/jxc/images/paswword.png")));
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 16));
		
		password = new JPasswordField();
		password.setFont(new Font("宋体", Font.PLAIN, 16));
		
		JButton submit = new JButton("登录");
		submit.addActionListener(this);//给登录按钮绑定点击事件
		submit.setIcon(new ImageIcon(LoginFrame.class.getResource("/com/bct/jxc/images/submit.png")));
		submit.setFont(new Font("宋体", Font.PLAIN, 14));
		
		JButton cancel = new JButton("退出");
		cancel.addActionListener(this);//给退出按钮绑定点击事件
		cancel.setIcon(new ImageIcon(LoginFrame.class.getResource("/com/bct/jxc/images/cancel.png")));
		cancel.setFont(new Font("宋体", Font.PLAIN, 14));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(89)
							.addComponent(submit, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(98)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_2)
								.addComponent(lblNewLabel_1))))
					.addGap(56)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(userName, GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
							.addComponent(password))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(cancel)
							.addGap(51)))
					.addContainerGap(52, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(124, Short.MAX_VALUE)
					.addComponent(lblNewLabel)
					.addGap(90))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(27)
					.addComponent(lblNewLabel)
					.addGap(34)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_1)
						.addComponent(userName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(32)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(password, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_2))
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancel)
						.addComponent(submit))
					.addContainerGap(44, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String btnCode = e.getActionCommand();//获取按钮上的文字
		switch (btnCode) {
		case "登录":
			String loginNamestr = userName.getText();
			String passwordStr = new String(password.getPassword());
			if(StringUtil.isEmpty(loginNamestr)) {
				JOptionPane.showMessageDialog(null, "账号不能为空","警告",JOptionPane.WARNING_MESSAGE);
			}else if(StringUtil.isEmpty(passwordStr)) {
				JOptionPane.showMessageDialog(null, "密码不能为空","警告",JOptionPane.WARNING_MESSAGE);
			}else {
				UserServer server = CommonFactory.getUserServer();
				boolean isPass = server.loginCheck(new User(loginNamestr, passwordStr));
				
				if(isPass) {
					MainFrame.loginName = loginNamestr;
					MainFrame.user = server.getUser(loginNamestr);//查询用户信息
					//JOptionPane.showMessageDialog(null, "登录成功！","警告",JOptionPane.INFORMATION_MESSAGE);
					MainFrame.getMainFrame().setVisible(true);//跳转到主界面
					this.setVisible(false);//隐藏登录界面
				}else {
					JOptionPane.showMessageDialog(null, "登录失败，账号或密码错误！","警告",JOptionPane.INFORMATION_MESSAGE);
				}
			}
			break;
		case "退出":
			System.exit(0);
			break;
		default:
			break;
		}
	}
	
}
