package com.bct.jxc.view;
/**
 * 客户查询信息
 */

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.StringUtil;
import com.bct.jxc.model.Customer;
import com.bct.jxc.server.CustomerServer;
import com.bct.jxc.server.impl.CustomerServerImpl;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import java.awt.Font;

public class CustomerInfoSearchFrame extends JInternalFrame implements ActionListener {
	private JTextField searchValue;
	private JComboBox comboBox;//查询选项
	private JTable table;//表格一定要放在JScrollPane，这样数据多的时候可以出现滚动条
	private MyTableModel tableModel;//自定义表格模板，为了适配数据
	private CustomerServer customerServer = null;

	/**
	 * 初始化视窗
	 */
	public CustomerInfoSearchFrame() {
		//支持放大缩小
		super("客户查询",true,true,true,true);
		//获取屏幕尺寸
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screenSize.width*2/3,screenSize.height*2/3);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("请选择查询条件：");
		lblNewLabel.setFont(new Font("楷体", Font.BOLD, 16));
		lblNewLabel.setIcon(new ImageIcon(CustomerInfoSearchFrame.class.getResource("/com/bct/jxc/images/customer_query.png")));
		panel.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("楷体", Font.BOLD, 14));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"客户ID", "客户全称", "客户简称", "联系人"}));
		panel.add(comboBox);
		
		searchValue = new JTextField();
		panel.add(searchValue);
		searchValue.setColumns(25);
		
		JButton searchOne = new JButton("查询");
		searchOne.setIcon(new ImageIcon(CustomerInfoSearchFrame.class.getResource("/com/bct/jxc/images/login.png")));
		searchOne.setFont(new Font("楷体", Font.BOLD, 16));
		//给查询绑定事件
		searchOne.addActionListener(this);
		panel.add(searchOne);
		
		JButton searchList = new JButton("显示全部信息");
		searchList.setIcon(new ImageIcon(CustomerInfoSearchFrame.class.getResource("/com/bct/jxc/images/provider_query.png")));
		searchList.setFont(new Font("楷体", Font.BOLD, 16));
		//给显示全部信息绑定事件
		searchList.addActionListener(this);
		panel.add(searchList);
		//创建表格的列（标题栏文字）
		String[] columNames = {"客户ID","客户全称","客户简称","客户地址","客户邮编","客户电话","客户传真"
				,"联系人","联系人电话","开户银行","银行账户","客户邮箱"};
		//实例化模板
		tableModel = new MyTableModel<Customer>(columNames, Customer.class);
		//创建表格面板
		table = new JTable(tableModel);
		table.setFont(new Font("宋体", Font.PLAIN, 12));
		//设置表格大小
		table.setPreferredScrollableViewportSize(new Dimension(screenSize.width*2/3-60, screenSize.height*2/3));
		//视图高度填充
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		//将表格放到滑动面板中
		JScrollPane scrollPane = new JScrollPane(table);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		//实例化server
		customerServer = CommonFactory.getCustomerService();
	}
	
	/**
	 * 点击事件
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String taget = e.getActionCommand();//获取按钮信息
		switch (taget) {
		case "查询":
			//搜索框非空验证
			String searchVal = searchValue.getText().trim();
			if(StringUtil.isEmpty(searchVal)) {
				JOptionPane.showMessageDialog(null, "请输入搜索的值","警告",JOptionPane.WARNING_MESSAGE);
				return;
			}
			String type = comboBox.getSelectedItem().toString();//获取查询类型
			Customer customer = new Customer();
			switch (type) {
			case "客户ID":
				customer.setId(searchVal);
				break;
			case "客户全称":
				customer.setCustomerName(searchVal);
				break;
			case "客户简称":
				customer.setShorts(searchVal);
				break;
			case "联系人":
				customer.setContracts(searchVal);
				break;
			default:
				break;
			}
			updateAllDate(customer);//更新数据
			break;
		case "显示全部信息":
			updateAllDate(null);//更新数据
			break;
		default:
			break;
		}
		
	}
	/**
	 * 更新数据
	 * @param customer
	 */
	private void updateAllDate(Customer customer) {
		//查询数据库
		List<Customer> customers = customerServer.findCustomers(customer);
		if(customers != null) {
			Vector<Customer> list = new Vector<Customer>();
			for (Customer c : customers) {
				list.add(c);
			}
			//更新数据
			tableModel.updateData(list);
			//清空输入框中的值
			searchValue.setText("");
		}
	}

}
