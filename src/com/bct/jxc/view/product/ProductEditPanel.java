package com.bct.jxc.view.product;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.Constants;
import com.bct.jxc.model.Product;
import com.bct.jxc.model.ProductInfo;
import com.bct.jxc.model.Provider;
import com.bct.jxc.model.Item;
import com.bct.jxc.server.ProductServer;
import com.bct.jxc.server.ProviderServer;
import com.bct.jxc.view.ProductInternalFrame;

import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

/**
 * 商品信息修改、删除面板
 * 
 * @author
 *
 */
public class ProductEditPanel extends JPanel implements ActionListener {
	private ProductServer productServer;
	private ProviderServer providerServer;
	private Item productItem;
	private JTextField nameTf;
	private JTextField shortsTf;
	private JTextField placeTf;
	private JTextField sizeTf;
	private JTextField packTf;
	private JTextField batchTf;
	private JTextField numberTf;
	private JTextField priceTf;
	private JTextField remarkTf;
	private JComboBox comboBox_1;
	private JComboBox comboBox;

	/**
	 * Create the panel.
	 */
	public ProductEditPanel() {

		JPanel panel_1 = new JPanel();
		add(panel_1);

		JLabel lblNewLabel_1 = new JLabel("商品全称：");
		lblNewLabel_1.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/provider.png")));
		lblNewLabel_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_1.add(lblNewLabel_1);

		nameTf = new JTextField();
		nameTf.setColumns(61);
		panel_1.add(nameTf);

		JPanel panel = new JPanel();
		add(panel);

		JLabel lblNewLabel = new JLabel("商品简称：");
		lblNewLabel.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/count.png")));
		lblNewLabel.setFont(new Font("楷体", Font.BOLD, 15));
		panel.add(lblNewLabel);

		shortsTf = new JTextField();
		shortsTf.setColumns(61);
		panel.add(shortsTf);

		JPanel panel_2 = new JPanel();
		add(panel_2);

		JLabel lblNewLabel_2 = new JLabel("商品产地：");
		lblNewLabel_2.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/address.png")));
		lblNewLabel_2.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2.add(lblNewLabel_2);

		placeTf = new JTextField();
		placeTf.setColumns(61);
		panel_2.add(placeTf);

		JPanel panel_3 = new JPanel();
		add(panel_3);

		JLabel lblNewLabel_3 = new JLabel("商品规格：");
		lblNewLabel_3.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/sales.png")));
		lblNewLabel_3.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3.add(lblNewLabel_3);

		sizeTf = new JTextField();
		sizeTf.setColumns(25);
		panel_3.add(sizeTf);

		JPanel panel_3_1 = new JPanel();
		add(panel_3_1);

		JLabel lblNewLabel_3_1 = new JLabel("商品包装：");
		lblNewLabel_3_1.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/chuanzhen.png")));
		lblNewLabel_3_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_1.add(lblNewLabel_3_1);

		packTf = new JTextField();
		packTf.setColumns(25);
		panel_3_1.add(packTf);

		JPanel panel_3_2 = new JPanel();
		add(panel_3_2);

		JLabel lblNewLabel_3_2 = new JLabel("商品批号：");
		lblNewLabel_3_2.setIcon(
				new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/entry_return_query.png")));
		lblNewLabel_3_2.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_2.add(lblNewLabel_3_2);

		batchTf = new JTextField();
		batchTf.setColumns(25);
		panel_3_2.add(batchTf);

		JPanel panel_3_1_1 = new JPanel();
		add(panel_3_1_1);

		JLabel lblNewLabel_3_1_1 = new JLabel("批准文号：");
		lblNewLabel_3_1_1.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/info.png")));
		lblNewLabel_3_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_1_1.add(lblNewLabel_3_1_1);

		numberTf = new JTextField();
		numberTf.setColumns(25);
		panel_3_1_1.add(numberTf);

		JPanel panel_2_1 = new JPanel();
		add(panel_2_1);

		JLabel lblNewLabel_2_1 = new JLabel("商品价格：");
		lblNewLabel_2_1.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/price.png")));
		lblNewLabel_2_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1.add(lblNewLabel_2_1);

		priceTf = new JTextField();
		priceTf.setColumns(61);
		panel_2_1.add(priceTf);

		JPanel panel_2_1_1 = new JPanel();
		add(panel_2_1_1);

		JLabel lblNewLabel_2_1_1 = new JLabel("商品备注：");
		lblNewLabel_2_1_1.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/number.png")));
		lblNewLabel_2_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1_1.add(lblNewLabel_2_1_1);

		remarkTf = new JTextField();
		remarkTf.setColumns(61);
		panel_2_1_1.add(remarkTf);

		JPanel panel_2_1_1_1 = new JPanel();
		add(panel_2_1_1_1);

		JLabel lblNewLabel_2_1_1_1 = new JLabel("供应商名称：");
		lblNewLabel_2_1_1_1
				.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/provider.png")));
		lblNewLabel_2_1_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1_1_1.add(lblNewLabel_2_1_1_1);

		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		comboBox.setPreferredSize(new Dimension(165, 21));
		comboBox.setMaximumRowCount(30);
		panel_2_1_1_1.add(comboBox);

		JPanel panel_2_1_1_1_1 = new JPanel();
		add(panel_2_1_1_1_1);

		JLabel lblNewLabel_2_1_1_1_1 = new JLabel("选择商品：");
		lblNewLabel_2_1_1_1_1
				.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/choose.png")));
		lblNewLabel_2_1_1_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1_1_1_1.add(lblNewLabel_2_1_1_1_1);

		comboBox_1 = new JComboBox();
		comboBox_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// 选择商品
				selectGoods();

			}
		});
		comboBox_1.setPreferredSize(new Dimension(170, 21));
		comboBox_1.setMaximumRowCount(30);
		panel_2_1_1_1_1.add(comboBox_1);

		JPanel panel_4 = new JPanel();
		add(panel_4);

		JButton saveBtn = new JButton("修改");
		saveBtn.addActionListener(this);
		saveBtn.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/save.png")));
		saveBtn.setFont(new Font("楷体", Font.BOLD, 15));
		panel_4.add(saveBtn);

		JButton btnNewButton = new JButton("删除");
		btnNewButton.addActionListener(this);
		btnNewButton.setIcon(new ImageIcon(ProductEditPanel.class.getResource("/com/bct/jxc/images/reset.png")));
		btnNewButton.setFont(new Font("楷体", Font.BOLD, 15));
		panel_4.add(btnNewButton);

		productServer = CommonFactory.getProductServer();
		providerServer = CommonFactory.getProviderServer();
		// 初始化商品下拉列表框
		initGoodsComboBox();
		// 初始化供应商下拉列表框
		initProviderComboBox();
	}

	/**
	 * 初始化商品下拉选择框
	 */
	public void initGoodsComboBox() {
		ProductInfo g;
		// 获取数据库中的数据
		List<ProductInfo> list = productServer.findProducts(null);
		// 设置下来列表框中数据模型
		Vector<Item> items = new Vector<>();
		items.clear();
		// 清空下拉列表框中的内容，免的重复
		comboBox_1.removeAllItems();
		// 迭代处理数据
		for (int i = 0; i < list.size(); i++) {
			Item item = new Item();
			item.setId(list.get(i).getId());
			item.setName(list.get(i).getProductName());
			if (items.contains(item)) {// 如果集合中包含该元素，就不在添加
				continue;
			}
			items.add(item);
			comboBox_1.addItem(item);
		}
	}


	/**
	 * 初始化供应商下拉选择框
	 */
	public void initProviderComboBox() {
		// 获取数据库中的数据
		List<Provider> list = providerServer.finds(null);
		// 设置下来列表框中数据模型
		Vector<Item> items = new Vector<>();
		// 清空下拉列表框中的内容，免的重复
		comboBox.removeAllItems();
		// 迭代处理数据
		for (int i = 0; i < list.size(); i++) {
			Item it = new Item();
			it.setId(list.get(i).getId());
			it.setName(list.get(i).getCustomerName());
			if (items.contains(it)) {// 如果集合中包含该元素，就不在添加
				continue;
			}
			items.add(it);
			comboBox.addItem(it);
		}
}

	/**
	 * 下拉列表框点击事件(商品)
	 */
	private void selectGoods() {
		if (!(comboBox_1.getSelectedItem() instanceof Item)) {
			return;
		}
		productItem = (Item) comboBox_1.getSelectedItem();
		System.out.println(productItem.getId() + "," + productItem.getName());
		// 根据商品编号查询数据库
		Product p = new Product();
		p.setId(productItem.getId());
		List<Product> list = productServer.findProducts(p);
		//更新界面
		setData(list.get(0));
	}
	/**
	 * 给组件添加数据
	 * 
	 * @param p
	 */
	private void setData(Product p) {
		nameTf.setText(p.getProductName());
		batchTf.setText(p.getBatch());
		numberTf.setText(p.getApprovalNumber());
		shortsTf.setText(p.getShorts());
		sizeTf.setText(p.getSpecification());
		placeTf.setText(p.getPlace());
		priceTf.setText(p.getPrice() + "");
		sizeTf.setText(p.getSpecification());
		remarkTf.setText(p.getRemark());
		packTf.setText(p.getPacking());
		//更新供应商下拉列表的值
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			Item providerItem = (Item) comboBox.getItemAt(i);
			System.out.println(i + " " + providerItem.getId() + "===" + p.getProviderId());
			if(providerItem.getId().equals(p.getProviderId())) {
				comboBox.setSelectedIndex(i);
			}
		}
	}

	/**
	 * 点击事件
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String target = e.getActionCommand();
		switch (target) {
		case "修改":
			// 获取商品编号
			String id = ((Item)(comboBox_1.getSelectedItem())).getId();//获取商品id
			String providerId = ((Item)(comboBox.getSelectedItem())).getId();//获取供应商id
			// 格式校验
			boolean flag = ProductInternalFrame.dataCheck(nameTf.getText().trim(), shortsTf.getText().trim(),
					placeTf.getText().trim(), sizeTf.getText().trim(), packTf.getText().trim(),
					batchTf.getText().trim(), numberTf.getText().trim(), priceTf.getText().trim(),
					remarkTf.getText().trim(), providerId.trim());
			if (flag) {
				// 修改数据
				boolean result = productServer.updateProduct(new Product(id, nameTf.getText().trim(),
						shortsTf.getText().trim(), placeTf.getText().trim(), sizeTf.getText().trim(),
						packTf.getText().trim(), batchTf.getText().trim(), numberTf.getText().trim(),
						Float.parseFloat(priceTf.getText().trim()), remarkTf.getText().trim(),providerId, 1));
				if (result) {
					JOptionPane.showMessageDialog(null, "商品信息修改成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "商品信息修改失败！", "警告", JOptionPane.WARNING_MESSAGE);
				}
			}

			break;
		case "删除":
			int type = JOptionPane.showConfirmDialog(null, "是否删除当前商品？", "温馨提示", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			System.out.println(type);
			if (type == 0) {// 删除
				// 获取商品编号
				//获取商品的Id
    			String sid =((Item)(comboBox_1.getSelectedItem())).getId();
				boolean r = productServer.updateProduct(sid, Constants.DATA_DEL);
				if (r) {
					JOptionPane.showMessageDialog(null, "商品信息删除成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
					// 更新界面
					initGoodsComboBox();
				} else {
					JOptionPane.showMessageDialog(null, "商品信息删除失败！", "警告", JOptionPane.WARNING_MESSAGE);
				}
			}
			break;
		default:
			break;
		}
	}
}
