package com.bct.jxc.view.product;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.model.Item;
import com.bct.jxc.model.Product;
import com.bct.jxc.model.Provider;
import com.bct.jxc.server.ProductServer;
import com.bct.jxc.server.ProviderServer;
import com.bct.jxc.view.CustomerInternalFrame;
import com.bct.jxc.view.ProductInternalFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import java.awt.Dimension;

/**
 * 商品信息添加面板
 * 
 * @author FPF
 *
 */
public class ProductAddPanel extends JPanel implements ActionListener {
	private ProductServer productServer = null;
	private ProviderServer providerServer;
	private JTextField shortsTf;
	private JTextField nameTf;
	private JTextField placeTf;
	private JTextField sizeTf;
	private JTextField packTf;
	private JTextField batchTf;
	private JTextField numberTf;
	private JTextField priceTf;
	private JTextField remarkTf;
	private JComboBox comboBox;
	private Item selectedItem;// 封装供应商编号对应的名称

	/**
	 * Create the panel.
	 */
	public ProductAddPanel() {

		JPanel panel_1 = new JPanel();
		add(panel_1);

		JLabel lblNewLabel_1 = new JLabel("商品全称：");
		lblNewLabel_1.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/goods.png")));
		lblNewLabel_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_1.add(lblNewLabel_1);

		nameTf = new JTextField();
		nameTf.setColumns(60);
		panel_1.add(nameTf);

		JPanel panel = new JPanel();
		add(panel);

		JLabel lblNewLabel = new JLabel("商品简称：");
		lblNewLabel.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/provider.png")));
		lblNewLabel.setFont(new Font("楷体", Font.BOLD, 15));
		panel.add(lblNewLabel);

		shortsTf = new JTextField();
		panel.add(shortsTf);
		shortsTf.setColumns(60);

		JPanel panel_2 = new JPanel();
		add(panel_2);

		JLabel lblNewLabel_2 = new JLabel("商品产地：");
		lblNewLabel_2.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/address.png")));
		lblNewLabel_2.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2.add(lblNewLabel_2);

		placeTf = new JTextField();
		placeTf.setColumns(60);
		panel_2.add(placeTf);

		JPanel panel_3 = new JPanel();
		add(panel_3);

		JLabel lblNewLabel_3 = new JLabel("商品规格：");
		lblNewLabel_3.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/sales.png")));
		lblNewLabel_3.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3.add(lblNewLabel_3);

		sizeTf = new JTextField();
		panel_3.add(sizeTf);
		sizeTf.setColumns(24);

		JPanel panel_3_1 = new JPanel();
		add(panel_3_1);

		JLabel lblNewLabel_3_1 = new JLabel("商品包装：");
		lblNewLabel_3_1.setIcon(
				new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/sells_return_query.png")));
		lblNewLabel_3_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_1.add(lblNewLabel_3_1);

		packTf = new JTextField();
		packTf.setColumns(24);
		panel_3_1.add(packTf);

		JPanel panel_3_2 = new JPanel();
		add(panel_3_2);

		JLabel lblNewLabel_3_2 = new JLabel("商品批号：");
		lblNewLabel_3_2.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/sells.png")));
		lblNewLabel_3_2.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_2.add(lblNewLabel_3_2);

		batchTf = new JTextField();
		batchTf.setColumns(24);
		panel_3_2.add(batchTf);

		JPanel panel_3_1_1 = new JPanel();
		add(panel_3_1_1);

		JLabel lblNewLabel_3_1_1 = new JLabel("批准文号：");
		lblNewLabel_3_1_1.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/number.png")));
		lblNewLabel_3_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_3_1_1.add(lblNewLabel_3_1_1);

		numberTf = new JTextField();
		numberTf.setColumns(24);
		panel_3_1_1.add(numberTf);

		JPanel panel_2_1 = new JPanel();
		add(panel_2_1);

		JLabel lblNewLabel_2_1 = new JLabel("商品价格：");
		lblNewLabel_2_1.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/price.png")));
		lblNewLabel_2_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1.add(lblNewLabel_2_1);

		priceTf = new JTextField();
		priceTf.setColumns(60);
		panel_2_1.add(priceTf);

		JPanel panel_2_1_1 = new JPanel();
		add(panel_2_1_1);

		JLabel lblNewLabel_2_1_1 = new JLabel("商品备注：");
		lblNewLabel_2_1_1.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/info.png")));
		lblNewLabel_2_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1_1.add(lblNewLabel_2_1_1);

		remarkTf = new JTextField();
		remarkTf.setColumns(60);
		panel_2_1_1.add(remarkTf);

		JPanel panel_2_1_1_1 = new JPanel();
		add(panel_2_1_1_1);

		JLabel lblNewLabel_2_1_1_1 = new JLabel("供应商名称：");
		lblNewLabel_2_1_1_1
				.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/provider_query.png")));
		lblNewLabel_2_1_1_1.setFont(new Font("楷体", Font.BOLD, 15));
		panel_2_1_1_1.add(lblNewLabel_2_1_1_1);

		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selectProviderAction();
			}
		});
		comboBox.setMaximumRowCount(30);
		comboBox.setPreferredSize(new Dimension(370, 21));
		panel_2_1_1_1.add(comboBox);
		
		JLabel label = new JLabel("");
		panel_2_1_1_1.add(label);
		
		JLabel label_1 = new JLabel("");
		panel_2_1_1_1.add(label_1);
		
		JLabel label_2 = new JLabel("");
		panel_2_1_1_1.add(label_2);
		
		JLabel label_3_2 = new JLabel("");
		panel_2_1_1_1.add(label_3_2);
		
		JLabel label_1_1_2 = new JLabel("");
		panel_2_1_1_1.add(label_1_1_2);
		
		JLabel label_2_1_2 = new JLabel("");
		panel_2_1_1_1.add(label_2_1_2);
		
		JLabel label_4_2 = new JLabel("");
		panel_2_1_1_1.add(label_4_2);
		
		JLabel label_1_2_2 = new JLabel("");
		panel_2_1_1_1.add(label_1_2_2);
		
		JLabel label_2_2_2 = new JLabel("");
		panel_2_1_1_1.add(label_2_2_2);
		
		JLabel label_5_2 = new JLabel("");
		panel_2_1_1_1.add(label_5_2);
		
		JLabel label_7 = new JLabel("");
		add(label_7);
		
		JLabel label_1_5 = new JLabel("");
		add(label_1_5);
		
		JLabel label_2_5 = new JLabel("");
		add(label_2_5);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JButton saveBtn = new JButton("保存");
		//给保存按钮绑定事件
		saveBtn.addActionListener(this);
		saveBtn.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/save.png")));
		saveBtn.setFont(new Font("楷体", Font.BOLD, 15));
		panel_4.add(saveBtn);
		
		JButton btnNewButton = new JButton("重置");
		//给保存按钮绑定事件
		btnNewButton.addActionListener(this);
		btnNewButton.setIcon(new ImageIcon(ProductAddPanel.class.getResource("/com/bct/jxc/images/im-reset.png")));
		btnNewButton.setFont(new Font("楷体", Font.BOLD, 15));
		panel_4.add(btnNewButton);
		// 初始化service
		productServer = CommonFactory.getProductServer();
		providerServer = CommonFactory.getProviderServer();
	}

	/**
	 * 清空文本框中的值
	 */
	private void setNull() {
		nameTf.setText("");
		shortsTf.setText("");
		batchTf.setText("");
		numberTf.setText("");
		placeTf.setText("");
		priceTf.setText("");
		sizeTf.setText("");
		remarkTf.setText("");
		packTf.setText("");
	}

	/**
	 * 初始化供应商下拉选择框
	 */
	public void initComboBox() {
		// 获取数据库中的数据
		List<Provider> list = providerServer.finds(null);
		// 设置下来列表框中数据模型
		Vector<Item> items = new Vector<>();
		// 清空下拉列表框中的内容，免的重复
		comboBox.removeAllItems();
		// 迭代处理数据
		if (list != null) {// 防止空指针
			for (int i = 0; i < list.size(); i++) {
				Item item = new Item();
				item.setId(list.get(i).getId());
				item.setName(list.get(i).getCustomerName());
				if (items.contains(item)) {// 如果集合中包含该元素，就不在添加
					continue;
				}
				items.add(item);
				comboBox.addItem(item);// 下拉框添加的是对象
			}
		}

	}

	/**
	 * 下拉列表框点击事件(供应商)
	 */
	private void selectProviderAction() {
		if (!(comboBox.getSelectedItem() instanceof Item)) {
			return;
		}
		selectedItem = (Item) comboBox.getSelectedItem();
		System.out.println(selectedItem.getId() + "," + selectedItem.getName());
	}

	/**
	 * 监听点击事件
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String target = e.getActionCommand();
		switch (target) {
		case "保存":

			boolean flag = ProductInternalFrame.dataCheck(nameTf.getText().trim(), shortsTf.getText().trim(),
					placeTf.getText().trim(), sizeTf.getText().trim(), packTf.getText().trim(),
					batchTf.getText().trim(), numberTf.getText().trim(), priceTf.getText().trim(),
					remarkTf.getText().trim(), selectedItem.getName().trim());
			if (flag) {
				// 获取商品编号
				String id = productServer.getProductId();
				// 将商品信息添加到数据库
				Product Product = new Product(id, shortsTf.getText().trim(), shortsTf.getText().trim(),
						placeTf.getText().trim(), sizeTf.getText().trim(), packTf.getText().trim(),
						batchTf.getText().trim(), numberTf.getText().trim(), Float.parseFloat(priceTf.getText().trim()),
						remarkTf.getText().trim(), selectedItem.getId().trim(), 1);
				boolean result = productServer.addProduct(Product);
				if (result == true) {
					JOptionPane.showMessageDialog(null, "商品信息添加成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
					setNull();
				} else {
					JOptionPane.showMessageDialog(null, "商品信息添加失败！", "警告", JOptionPane.WARNING_MESSAGE);
				}

			}

			break;
		case "重置":
			setNull();
			break;
		default:
			break;
		}

	}
}
