package com.bct.jxc.view;

import java.lang.reflect.Method;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import com.bct.jxc.common.ErrorManager;
import com.bct.jxc.model.Customer;

/**
 * 自定义表格数据模板
 * @author ASUS
 *
 */

public class MyTableModel<T> extends AbstractTableModel{
	private Vector<T> data = new Vector<T>();//数据源，这里支持Vector
	private String[] columNames;//表格中的标题栏数字
	private Class<T> cls;//实例
	
	public MyTableModel(String[] columNames,Class<T> cls) {
		super();
		this.columNames = columNames;
		this.cls = cls;
	}

	/**
	 * 生成行的数量
	 */
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return data.size();
	}
	/**
	 * 生成列的数量
	 */
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columNames.length;
	}
	/**
	 * 获取对应列的列名
	 */
	@Override
	public String getColumnName(int column) {
		return columNames[column];
	}
	/**
	 * 获取对应列对应行的数据
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		T t = (T) data.get(rowIndex);//一行就是一个对象(对应行的信息)
		try {
			//通过反射拿到“GetView”方法
			Method method = cls.getMethod("getValue", Integer.class);//方法及方法参数类型
			method.setAccessible(true);
			return method.invoke(t, columnIndex);
		} catch (Exception e) {
			ErrorManager.printError("MyTableModel getValue()", e);
		}
		return null;
	}
	/**
	 * 设置表格是否可编辑
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	/**
	 * 每一行对应的类
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex).getClass();
	}
	/**
	 * 自定义，用来更新数据
	 * @param customers
	 */
	public void updateData(Vector<T> data) {
		this.data = data;//更新数据
		if(data.size() == 0) {
			data = new Vector<T>();
		}else {
			//自动填充数据
			fireTableRowsInserted(0, data.size() - 1);
		}
	}
}