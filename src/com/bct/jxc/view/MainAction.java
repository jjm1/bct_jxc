package com.bct.jxc.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.bct.jxc.model.Product;
import com.bct.jxc.model.Provider;

public class MainAction {
	/**
	 *  点击跳转到“客户信息管理界面”
	 * @return
	 */
	public static ActionListener clickCustomerInfoManager() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				CustomerInternalFrame cuInternalFrame = new CustomerInternalFrame();
				MainFrame.getMainFrame().getContentPane().add(cuInternalFrame);//将此面板添加到主界面上
				cuInternalFrame.setVisible(true);				
			}
		};
	}
	/**
	 * 点击跳转到“客户查询界面”
	 * @return
	 */
	public static ActionListener clickCustomerInfoSearch() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				CustomerInfoSearchFrame infoSearchFrame = new CustomerInfoSearchFrame();
				MainFrame.getMainFrame().getContentPane().add(infoSearchFrame);
				infoSearchFrame.setVisible(true);
			}
		};
	}
	/**
	 *  点击跳转到“供应商信息管理界面”
	 * @return
	 */
	public static ActionListener clickProviderInfoManager() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ProviderInternalFrame cuInternalFrame = new ProviderInternalFrame();
				MainFrame.getMainFrame().getContentPane().add(cuInternalFrame);//将此面板添加到主界面上
				cuInternalFrame.setVisible(true);				
			}
		};
	}
	/**
	 * 点击跳转到“供应商查询界面”
	 * @return
	 */
	public static ActionListener clickProviderInfoSearch() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ProviderInfoSearchFrame infoSearchFrame = new ProviderInfoSearchFrame();
				MainFrame.getMainFrame().getContentPane().add(infoSearchFrame);
				infoSearchFrame.setVisible(true);
			}
		};
	}
	/**
	 * 点击跳转到“商品查询界面”
	 * @return
	 */
	public static ActionListener clickProductInfoSearch() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ProductInfoSearchFrame infoSearchFrame = new ProductInfoSearchFrame();
				MainFrame.getMainFrame().getContentPane().add(infoSearchFrame);
				infoSearchFrame.setVisible(true);
			}
		};
	}
	/**
	 *  点击跳转到“商品信息管理界面”
	 * @return
	 */
	public static ActionListener clickProductInfoManager() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ProductInternalFrame cuInternalFrame = new ProductInternalFrame();
				MainFrame.getMainFrame().getContentPane().add(cuInternalFrame);//将此面板添加到主界面上
				cuInternalFrame.setVisible(true);				
			}
		};
	}
	/**
	 *  点击跳转到“进货单信息管理界面”
	 * @return
	 */
	public static ActionListener clickInportInfoManager() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				InportInternalFrame cuInternalFrame = new InportInternalFrame();
				MainFrame.getMainFrame().getContentPane().add(cuInternalFrame);//将此面板添加到主界面上
				cuInternalFrame.setVisible(true);				
			}
		};
	}
}
