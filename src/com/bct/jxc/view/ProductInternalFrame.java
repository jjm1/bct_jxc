package com.bct.jxc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import com.bct.jxc.common.StringUtil;
import com.bct.jxc.view.product.ProductAddPanel;
import com.bct.jxc.view.product.ProductEditPanel;
import java.awt.Color;

/**
 * 商品信息管理界面（添加、修改、删除）
 * 
 * @author FPF
 *
 */
public class ProductInternalFrame extends JInternalFrame {

	/**
	 * 初始化视图
	 */
	public ProductInternalFrame() {
		setFrameIcon(new ImageIcon(ProductInternalFrame.class.getResource("/com/bct/jxc/images/logo-s.png")));
		setTitle("商品信息管理");
		setIconifiable(true);// 支持窗口最小化显示
		setClosable(true);// 支持窗口可关闭
		getContentPane().setLayout(new BorderLayout(0, 0));
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width/12, screenSize.height/20, screenSize.width/5, screenSize.height/2);
		// 创建选项卡
		JTabbedPane tabPane = new JTabbedPane();
		tabPane.setForeground(Color.BLACK);
		final ProductAddPanel addPanel = new ProductAddPanel();// 商品信息添加面板
		final ProductEditPanel editPanel = new ProductEditPanel();// 商品信息编辑面板
		// 向选项卡添加面板
		tabPane.addTab("商品信息添加", null, addPanel, "商品信息添加");
		addPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		tabPane.addTab("商品信息编辑", null, editPanel, "商品信息编辑");
		// 向Frame窗口添加选项卡
		getContentPane().add(tabPane);
		// 给选项卡设置改变监听
		tabPane.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				// 初始化编辑界面的下拉列表框
				editPanel.initGoodsComboBox();
				editPanel.initProviderComboBox();
			}
		});
		//在商品管理窗口被激活时，初始化商品添加界面的供应商下拉选择
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameActivated(InternalFrameEvent e) {
				addPanel.initComboBox();
			}
		});
		pack();//通过内部组件自动设置窗口大小
		setVisible(true);// 显示窗口
	}

	/**
	 * 数据格式校验
	 * 
	 * @param name
	 * @param address
	 * @param shorts
	 * @param zip
	 * @param tel
	 * @param fax
	 * @param contacts
	 * @param phone
	 * @param email
	 * @param bank
	 * @param account
	 */
	public static boolean dataCheck(String name, String shorts, String palce, String size, String pack, String batch,
			String numbers, String price, String remark, String provider) {
		if (StringUtil.isEmpty(name) || StringUtil.isEmpty(shorts) || StringUtil.isEmpty(palce)
				|| StringUtil.isEmpty(size) || StringUtil.isEmpty(pack) || StringUtil.isEmpty(batch)
				|| StringUtil.isEmpty(numbers) || StringUtil.isEmpty(price) || StringUtil.isEmpty(remark)
				|| StringUtil.isEmpty(provider)) {
			JOptionPane.showMessageDialog(null, "请填写全部信息！", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		if (!StringUtil.isPrice(price)) {
			JOptionPane.showMessageDialog(null, "价格不合法！", "警告", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}
}
