package com.bct.jxc.view;
/**
 * 商品查询信息
 */

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.StringUtil;
import com.bct.jxc.model.Customer;
import com.bct.jxc.model.Product;
import com.bct.jxc.model.ProductInfo;
import com.bct.jxc.server.CustomerServer;
import com.bct.jxc.server.ProductServer;
import com.bct.jxc.server.impl.CustomerServerImpl;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import java.awt.Font;

public class ProductInfoSearchFrame extends JInternalFrame implements ActionListener {
	private JTextField searchValue;
	private JComboBox comboBox;//查询选项
	private JTable table;//表格一定要放在JScrollPane，这样数据多的时候可以出现滚动条
	private MyTableModel tableModel;//自定义表格模板，为了适配数据
	private ProductServer productServer = null;

	/**
	 * 初始化视窗
	 */
	public ProductInfoSearchFrame() {
		//支持放大缩小
		super("商品查询",true,true,true,true);
		//获取屏幕尺寸
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screenSize.width*2/3,screenSize.height*2/3);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("请选择查询条件：");
		lblNewLabel.setFont(new Font("楷体", Font.BOLD, 16));
		lblNewLabel.setIcon(new ImageIcon(ProductInfoSearchFrame.class.getResource("/com/bct/jxc/images/customer_query.png")));
		panel.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("楷体", Font.BOLD, 14));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"商品ID", "商品全称", "商品简称", "供应商全称"}));
		panel.add(comboBox);
		
		searchValue = new JTextField();
		panel.add(searchValue);
		searchValue.setColumns(25);
		
		JButton searchOne = new JButton("查询");
		searchOne.setIcon(new ImageIcon(ProductInfoSearchFrame.class.getResource("/com/bct/jxc/images/login.png")));
		searchOne.setFont(new Font("楷体", Font.BOLD, 16));
		//给查询绑定事件
		searchOne.addActionListener(this);
		panel.add(searchOne);
		
		JButton searchList = new JButton("显示全部信息");
		searchList.setIcon(new ImageIcon(ProductInfoSearchFrame.class.getResource("/com/bct/jxc/images/provider_query.png")));
		searchList.setFont(new Font("楷体", Font.BOLD, 16));
		//给显示全部信息绑定事件
		searchList.addActionListener(this);
		panel.add(searchList);
		//创建表格的列（标题栏文字）
		String[] columNames = {"商品ID","商品全称","商品简称","商品产地","商品规格","商品包装","商品批号"
				,"批准文号","商品价格","商品备注","供应商全称"};
		//实例化模板
		tableModel = new MyTableModel<Product>(columNames, Product.class);
		//创建表格面板
		table = new JTable(tableModel);
		table.setFont(new Font("宋体", Font.PLAIN, 12));
		//设置表格大小
		table.setPreferredScrollableViewportSize(new Dimension(screenSize.width*2/3-80, screenSize.height*2/3));
		//视图高度填充
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		//将表格放到滑动面板中
		JScrollPane scrollPane = new JScrollPane(table);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		//实例化server
		productServer = CommonFactory.getProductServer();
	}
	
	/**
	 * 点击事件
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String taget = e.getActionCommand();//获取按钮信息
		switch (taget) {
		case "查询":
			//搜索框非空验证
			String searchVal = searchValue.getText().trim();
			if(StringUtil.isEmpty(searchVal)) {
				JOptionPane.showMessageDialog(null, "请输入搜索的值","警告",JOptionPane.WARNING_MESSAGE);
				return;
			}
			String type = comboBox.getSelectedItem().toString();//获取查询类型
			ProductInfo product = new ProductInfo();
			switch (type) {
			case "商品ID":
				product.setId(searchVal);
				break;
			case "商品全称":
				product.setProductName(searchVal);
				break;
			case "商品简称":
				product.setShorts(searchVal);
				break;
			case "供应商全称":
				product.setProviderName(searchVal);
				break;
			default:
				break;
			}
			updateAllDate(product);//更新数据
			break;
		case "显示全部信息":
			updateAllDate(null);//更新数据
			break;
		default:
			break;
		}
		
	}
	/**
	 * 更新数据
	 * @param product
	 */
	private void updateAllDate(ProductInfo product) {
		//查询数据库
		List<ProductInfo> products = productServer.findProducts(product);
		if(products != null) {
			Vector<ProductInfo> list = new Vector<ProductInfo>();
			for (ProductInfo c : products) {
				list.add(c);
			}
			//更新数据
			tableModel.updateData(list);
			//清空输入框中的值
			searchValue.setText("");
		}
	}

}
