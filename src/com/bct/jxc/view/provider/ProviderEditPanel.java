package com.bct.jxc.view.provider;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.common.Constants;
import com.bct.jxc.model.Provider;
import com.bct.jxc.model.Item;
import com.bct.jxc.server.ProviderServer;
import com.bct.jxc.view.CustomerInternalFrame;

import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
/**
 * 供应商信息修改、删除面板
 * @author 
 *
 */
public class ProviderEditPanel extends JPanel implements ActionListener{
	private ProviderServer ProviderServer;
	private JComboBox comboBox;
	private JTextField nameTf;
	private JTextField addressTf;
	private JTextField shortTf;
	private JTextField zipTf;
	private JTextField telTf;
	private JTextField faxTf;
	private JTextField conTf;
	private JTextField phoneTf;
	private JTextField bankTf;
	private JTextField accountTf;
	private JTextField emailTf;
	private Item selectedItem;

	/**
	 * Create the panel.
	 */
	public ProviderEditPanel() {
		JPanel panel = new JPanel();
		
		JLabel label = new JLabel("供应商全称：");
		label.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/full_name.png")));
		label.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel.add(label);
		
		nameTf = new JTextField();
		nameTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		nameTf.setColumns(16);
		panel.add(nameTf);
		
		JPanel panel_1 = new JPanel();
		
		JLabel label_1 = new JLabel("供应商地址：");
		label_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/address.png")));
		label_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_1.add(label_1);
		
		addressTf = new JTextField();
		addressTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		addressTf.setColumns(16);
		panel_1.add(addressTf);
		
		JPanel panel_2 = new JPanel();
		
		JLabel label_2 = new JLabel("供应商简称：");
		label_2.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/shorts.png")));
		label_2.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_2.add(label_2);
		
		shortTf = new JTextField();
		shortTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		shortTf.setColumns(16);
		panel_2.add(shortTf);
		
		JPanel panel_1_1 = new JPanel();
		
		JLabel label_1_1 = new JLabel("供应商邮编：");
		label_1_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/zip.png")));
		label_1_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_1_1.add(label_1_1);
		
		zipTf = new JTextField();
		zipTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		zipTf.setColumns(16);
		panel_1_1.add(zipTf);
		
		JPanel panel_2_1 = new JPanel();
		
		JLabel label_3 = new JLabel("供应商电话：");
		label_3.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/tel.png")));
		label_3.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_2_1.add(label_3);
		
		telTf = new JTextField();
		telTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		telTf.setColumns(16);
		panel_2_1.add(telTf);
		
		JPanel panel_1_1_1 = new JPanel();
		
		JLabel label_1_1_1 = new JLabel("供应商传真：");
		label_1_1_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/chuanzhen.png")));
		label_1_1_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_1_1_1.add(label_1_1_1);
		
		faxTf = new JTextField();
		faxTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		faxTf.setColumns(16);
		panel_1_1_1.add(faxTf);
		
		JPanel panel_2_1_1 = new JPanel();
		
		JLabel label_2_1_1 = new JLabel("联系人员：");
		label_2_1_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/contacts.png")));
		label_2_1_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_2_1_1.add(label_2_1_1);
		
		conTf = new JTextField();
		conTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		conTf.setColumns(16);
		panel_2_1_1.add(conTf);
		
		JPanel panel_1_1_1_1 = new JPanel();
		
		JLabel label_1_1_1_1 = new JLabel("联系电话：");
		label_1_1_1_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/contacts_tel.png")));
		label_1_1_1_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_1_1_1_1.add(label_1_1_1_1);
		
		phoneTf = new JTextField();
		phoneTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		phoneTf.setColumns(16);
		panel_1_1_1_1.add(phoneTf);
		
		JPanel panel_2_1_2 = new JPanel();
		
		JLabel label_3_1 = new JLabel("开户银行：");
		label_3_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/bank.png")));
		label_3_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_2_1_2.add(label_3_1);
		
		bankTf = new JTextField();
		bankTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		bankTf.setColumns(16);
		panel_2_1_2.add(bankTf);
		
		JPanel panel_1_1_1_1_1 = new JPanel();
		
		JLabel label_1_1_1_1_1 = new JLabel("银行账户：");
		label_1_1_1_1_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/account.png")));
		label_1_1_1_1_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_1_1_1_1_1.add(label_1_1_1_1_1);
		
		accountTf = new JTextField();
		accountTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		accountTf.setColumns(16);
		panel_1_1_1_1_1.add(accountTf);
		
		JPanel panel_3 = new JPanel();
		
		JLabel lblNewLabel = new JLabel("供应商邮箱：");
		lblNewLabel.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/mail.png")));
		lblNewLabel.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_3.add(lblNewLabel);
		
		emailTf = new JTextField();
		emailTf.setFont(new Font("华文楷体", Font.BOLD, 16));
		emailTf.setColumns(30);
		panel_3.add(emailTf);
		
		JPanel panel_3_1 = new JPanel();
		
		JLabel lblNewLabel_1 = new JLabel("选择供应商：");
		lblNewLabel_1.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/choose.png")));
		lblNewLabel_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_3_1.add(lblNewLabel_1);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("华文楷体", Font.BOLD, 16));
		comboBox.setMaximumRowCount(30);
        comboBox.setPreferredSize(new Dimension(200, 21));
        //给comboBox设置选中事件
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectAction();
			}

		});
		panel_3_1.add(comboBox);
		
		JPanel panel_4 = new JPanel();
		
		JPanel panel_4_1 = new JPanel();
		
		JButton deleteBtn = new JButton("删除");
		deleteBtn.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/delete.png")));
		deleteBtn.setFont(new Font("华文楷体", Font.BOLD, 16));
		//给删除按钮绑定点击事件
		deleteBtn.addActionListener(this);
		panel_4_1.add(deleteBtn);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 456, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1_1, GroupLayout.PREFERRED_SIZE, 456, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_2_1, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1_1_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_2_1_1, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1_1_1_1, GroupLayout.PREFERRED_SIZE, 456, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_2_1_2, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1_1_1_1_1, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE)
							.addGap(66)
							.addComponent(panel_4_1, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))
						.addComponent(panel_3_1, GroupLayout.PREFERRED_SIZE, 815, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 608, GroupLayout.PREFERRED_SIZE))
					.addGap(57))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1_1_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2_1_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1_1_1_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2_1_2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1_1_1_1_1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_3_1, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_4_1, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(106, Short.MAX_VALUE))
		);
		
		JButton updateBtn = new JButton("修改");
		updateBtn.setIcon(new ImageIcon(ProviderEditPanel.class.getResource("/com/bct/jxc/images/change.png")));
		updateBtn.setFont(new Font("华文楷体", Font.BOLD, 16));
		panel_4.add(updateBtn);
		//给修改按钮绑定点击事件
		updateBtn.addActionListener(this);
		panel_4.add(updateBtn);
		setLayout(groupLayout);
        
		ProviderServer =  CommonFactory.getProviderServer();
		//初始化供应商下拉选择框
		initComboBox();
	}
	/**
	 * 初始化供应商下拉选择框
	 */
	public void initComboBox() {
		//获取数据库中的数据
		List<Provider> list = ProviderServer.finds(null);
		//设置下来列表框中数据模型
		Vector<Item> items = new Vector<>();
		//清空下拉列表框中的内容，免的重复
		comboBox.removeAllItems();
		//迭代处理数据
		if(list != null) {//防止空指针
			for(int i=0;i<list.size();i++) {
				Item item = new Item();
				item.setId(list.get(i).getId());
				item.setName(list.get(i).getCustomerName());
				if(items.contains(item)) {//如果集合中包含该元素，就不在添加
					continue;			
				}
				items.add(item);	
				comboBox.addItem(item);//下拉框添加的是对象
			}
		}
		
	}
	/**
	 * 下拉列表框点击事件
	 */
	private void selectAction() {
		Item selectedItem;
		if(!(comboBox.getSelectedItem() instanceof Item)) {
			return;
		}
		selectedItem = (Item) comboBox.getSelectedItem();
		System.out.println(selectedItem.getId()+","+selectedItem.getName());
		//根据供应商编号查询数据库
		Provider c = new Provider();
		c.setId(selectedItem.getId());
		List<Provider> cust = ProviderServer.finds(c);
		setData(cust.get(0));//该集合不可能重复，只有一条
		
	}
	/**
	 * 给组件添加数据
	 * @param c
	 */
	private void setData(Provider c) {
		nameTf.setText(c.getCustomerName());
		addressTf.setText(c.getAddress());
		shortTf.setText(c.getShorts());
		zipTf.setText(c.getZip());
		telTf.setText(c.getTelephone());
		faxTf.setText(c.getFax());
		conTf.setText(c.getContracts());
		phoneTf.setText(c.getContractsTele());
		bankTf.setText(c.getBank());
		accountTf.setText(c.getBank());
		emailTf.setText(c.getEmail());
	}
	/**
	 * 点击事件
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		selectedItem = (Item) comboBox.getSelectedItem();
		String target = e.getActionCommand();
		System.out.println(selectedItem.getId());
		System.out.println(123);
		System.out.println(target);
		switch (target) {
		case "修改":
			//获取供应商编号
			String id = selectedItem.getId();
			//格式校验
			boolean flag = CustomerInternalFrame.dataCheck(nameTf.getText().trim(),
					addressTf.getText().trim(),
					shortTf.getText().trim(),
					zipTf.getText().trim(),
					telTf.getText().trim(),
					faxTf.getText().trim(),
					conTf.getText().trim(),
					phoneTf.getText().trim(),
					emailTf.getText().trim(),
					bankTf.getText().trim(),
					accountTf.getText().trim());
			if(flag) {
				//修改数据			
				boolean result = ProviderServer.updateProvider(new Provider(id, 
						nameTf.getText().trim(), shortTf.getText().trim(),addressTf.getText().trim(), 
						zipTf.getText().trim(), 
						telTf.getText().trim(), faxTf.getText().trim(), 
						conTf.getText().trim(), phoneTf.getText().trim(), 
						bankTf.getText().trim(), accountTf.getText(), 
						emailTf.getText().trim(), 1));
				if(result) {
					JOptionPane.showMessageDialog(null, "供应商信息修改成功！","提示",JOptionPane.INFORMATION_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(null, "供应商信息修改失败！","警告",JOptionPane.WARNING_MESSAGE);
				}
			}
			
			break;
		case "删除":
			int type = JOptionPane.showConfirmDialog(null, "是否删除当前供应商？", "温馨提示", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			System.out.println(selectedItem.getId());
			if(type==0) {//删除
				//获取供应商编号
				String sid = selectedItem.getId();
				boolean r = ProviderServer.updateProvider(sid, Constants.DATA_DEL);
				if(r) {
					JOptionPane.showMessageDialog(null, "供应商信息删除成功！","提示",JOptionPane.INFORMATION_MESSAGE);
					//更新界面
					initComboBox();
				}else {
					JOptionPane.showMessageDialog(null, "供应商信息删除失败！","警告",JOptionPane.WARNING_MESSAGE);
				}
			}
			break;
		default:
			break;
		}
	}
}
