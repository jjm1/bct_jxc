package com.bct.jxc.view.provider;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.bct.jxc.common.CommonFactory;
import com.bct.jxc.model.Provider;
import com.bct.jxc.server.ProviderServer;
import com.bct.jxc.view.CustomerInternalFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 * 供应商信息添加面板
 * 
 * @author FPF
 *
 */
public class ProviderAddPanel extends JPanel implements ActionListener {
	private JTextField ProviderNameTf;
	private JTextField addressTf;
	private JTextField shortsTf;
	private JTextField zipTf;
	private JTextField telTf;
	private JTextField faxTf;
	private JTextField contactsTf;
	private JTextField phoneTf;
	private JTextField accountTf;
	private ProviderServer ProviderServer = null;
	private JTextField bankTf;
	private JTextField emailTf;

	/**
	 * Create the panel.
	 */
	public ProviderAddPanel() {
		JPanel panel = new JPanel();
				
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		add(panel);
		
		JLabel lblNewLabel = new JLabel("供应商全称：");
		lblNewLabel.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/Provider_query.png")));
		lblNewLabel.setFont(new Font("楷体", Font.BOLD, 16));
		panel.add(lblNewLabel);
		
		ProviderNameTf = new JTextField();
		panel.add(ProviderNameTf);
		ProviderNameTf.setColumns(25);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel lblNewLabel_1 = new JLabel("供应商地址：");
		lblNewLabel_1.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/address.png")));
		lblNewLabel_1.setFont(new Font("楷体", Font.BOLD, 16));
		panel_1.add(lblNewLabel_1);
		
		addressTf = new JTextField();
		panel_1.add(addressTf);
		addressTf.setColumns(25);
		
		JLabel label_14 = new JLabel("");
		add(label_14);
		
		JLabel label_15 = new JLabel("");
		add(label_15);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel lblNewLabel_2 = new JLabel("供应商简称：");
		lblNewLabel_2.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/shorts.png")));
		lblNewLabel_2.setFont(new Font("楷体", Font.BOLD, 16));
		panel_2.add(lblNewLabel_2);
		
		shortsTf = new JTextField();
		panel_2.add(shortsTf);
		shortsTf.setColumns(25);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JLabel lblNewLabel_3 = new JLabel("供应商邮编：");
		lblNewLabel_3.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/Provider.png")));
		lblNewLabel_3.setFont(new Font("楷体", Font.BOLD, 16));
		panel_3.add(lblNewLabel_3);
		
		zipTf = new JTextField();
		panel_3.add(zipTf);
		zipTf.setColumns(25);
		
		JLabel label_3 = new JLabel("");
		add(label_3);
		
		JLabel label_21 = new JLabel("");
		add(label_21);
		
		JLabel label_22 = new JLabel("");
		add(label_22);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JLabel lblNewLabel_4 = new JLabel("供应商电话：");
		lblNewLabel_4.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/phone on.png")));
		lblNewLabel_4.setFont(new Font("楷体", Font.BOLD, 16));
		panel_4.add(lblNewLabel_4);
		
		telTf = new JTextField();
		panel_4.add(telTf);
		telTf.setColumns(25);
		
		JLabel label_26_1 = new JLabel("");
		panel_4.add(label_26_1);
		
		JLabel label_27_1 = new JLabel("");
		panel_4.add(label_27_1);
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		
		JLabel lblNewLabel_5 = new JLabel("传     真：");
		lblNewLabel_5.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/fax.png")));
		lblNewLabel_5.setFont(new Font("楷体", Font.BOLD, 16));
		panel_5.add(lblNewLabel_5);
		
		faxTf = new JTextField();
		panel_5.add(faxTf);
		faxTf.setColumns(25);
		
		JLabel label_26 = new JLabel("");
		add(label_26);
		
		JLabel label_27 = new JLabel("");
		add(label_27);
		
		JPanel panel_6 = new JPanel();
		add(panel_6);
		
		JLabel lblNewLabel_6 = new JLabel("联 系 人：");
		lblNewLabel_6.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/contacts.png")));
		lblNewLabel_6.setFont(new Font("楷体", Font.BOLD, 16));
		panel_6.add(lblNewLabel_6);
		
		contactsTf = new JTextField();
		panel_6.add(contactsTf);
		contactsTf.setColumns(25);
		
		JLabel label_4 = new JLabel("");
		panel_6.add(label_4);
		
		JLabel label_4_2 = new JLabel("");
		panel_6.add(label_4_2);
		
		JLabel label_4_1 = new JLabel("");
		panel_6.add(label_4_1);
		
		JPanel panel_7 = new JPanel();
		add(panel_7);
		
		JLabel lblNewLabel_7 = new JLabel("联系电话：");
		lblNewLabel_7.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/contacts_tel.png")));
		lblNewLabel_7.setFont(new Font("楷体", Font.BOLD, 16));
		panel_7.add(lblNewLabel_7);
		
		phoneTf = new JTextField();
		panel_7.add(phoneTf);
		phoneTf.setColumns(25);
		
		JLabel label_43_1 = new JLabel("");
		panel_7.add(label_43_1);
		
		JLabel label_43 = new JLabel("");
		add(label_43);
		
		JLabel label_5 = new JLabel("");
		add(label_5);
		
		JPanel panel_9 = new JPanel();
		add(panel_9);
		
		JPanel panel_10 = new JPanel();
		panel_9.add(panel_10);
		
		JLabel lblNewLabel_9 = new JLabel("开户银行：");
		lblNewLabel_9.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/bank.png")));
		lblNewLabel_9.setFont(new Font("楷体", Font.BOLD, 16));
		panel_10.add(lblNewLabel_9);
		
		bankTf = new JTextField();
		panel_10.add(bankTf);
		bankTf.setColumns(25);
		
		JLabel label_26_2 = new JLabel("");
		panel_9.add(label_26_2);
		
		JLabel label_27_2 = new JLabel("");
		panel_9.add(label_27_2);
		
		JPanel panel_11 = new JPanel();
		add(panel_11);
		
		JLabel lblNewLabel_10 = new JLabel("银行账户：");
		lblNewLabel_10.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/account.png")));
		lblNewLabel_10.setFont(new Font("楷体", Font.BOLD, 16));
		panel_11.add(lblNewLabel_10);
		
		accountTf = new JTextField();
		panel_11.add(accountTf);
		accountTf.setColumns(25);
		
		JLabel label_2 = new JLabel("");
		add(label_2);
		
		JLabel label_1 = new JLabel("");
		add(label_1);
		
		JLabel label = new JLabel("");
		add(label);
		
		JPanel panel_8 = new JPanel();
		add(panel_8);
		
		JLabel lblNewLabel_8 = new JLabel("供应商邮箱：");
		lblNewLabel_8.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/Email Hili.png")));
		lblNewLabel_8.setFont(new Font("楷体", Font.BOLD, 16));
		panel_8.add(lblNewLabel_8);
		
		emailTf = new JTextField();
		emailTf.setColumns(50);
		panel_8.add(emailTf);
		
		JLabel label_13_1 = new JLabel("");
		panel_8.add(label_13_1);
		
		JLabel label_12_1 = new JLabel("");
		panel_8.add(label_12_1);
		
		JLabel label_11_1 = new JLabel("");
		panel_8.add(label_11_1);
		
		JLabel label_16_2 = new JLabel("");
		panel_8.add(label_16_2);
		
		JLabel label_13_2 = new JLabel("");
		panel_8.add(label_13_2);
		
		JLabel label_12_2 = new JLabel("");
		panel_8.add(label_12_2);
		
		JLabel label_11_2 = new JLabel("");
		panel_8.add(label_11_2);
		
		JLabel label_16_3 = new JLabel("");
		panel_8.add(label_16_3);
		
		JLabel label_13_3 = new JLabel("");
		panel_8.add(label_13_3);
		
		JLabel label_12_3 = new JLabel("");
		panel_8.add(label_12_3);
		
		JLabel label_11_3 = new JLabel("");
		panel_8.add(label_11_3);
		
		JLabel label_16_1_1 = new JLabel("");
		panel_8.add(label_16_1_1);
		
		JLabel label_13_1_1 = new JLabel("");
		panel_8.add(label_13_1_1);
		
		JLabel label_12_1_1 = new JLabel("");
		panel_8.add(label_12_1_1);
		
		JLabel label_11_1_1 = new JLabel("");
		panel_8.add(label_11_1_1);
		
		JLabel label_16_2_1 = new JLabel("");
		panel_8.add(label_16_2_1);
		
		JLabel label_16_1_2 = new JLabel("");
		panel_8.add(label_16_1_2);
		
		JLabel label_13_1_2 = new JLabel("");
		panel_8.add(label_13_1_2);
		
		JLabel label_12_1_2 = new JLabel("");
		panel_8.add(label_12_1_2);
		
		JLabel label_11_1_2 = new JLabel("");
		panel_8.add(label_11_1_2);
		
		JLabel label_16_2_2 = new JLabel("");
		panel_8.add(label_16_2_2);
		
		JLabel label_16_1_3 = new JLabel("");
		panel_8.add(label_16_1_3);
		
		JLabel label_13_1_3 = new JLabel("");
		panel_8.add(label_13_1_3);
		
		JLabel label_17 = new JLabel("");
		add(label_17);
		
		JLabel label_16 = new JLabel("");
		add(label_16);
		
		JLabel label_13 = new JLabel("");
		add(label_13);
		
		JLabel label_12 = new JLabel("");
		add(label_12);
		
		JLabel label_11 = new JLabel("");
		add(label_11);
		
		JLabel label_9 = new JLabel("");
		add(label_9);
		
		JPanel panel_12 = new JPanel();
		add(panel_12);
		
		JButton saveBtn = new JButton("保存");
		saveBtn.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/save.png")));
		saveBtn.setFont(new Font("楷体", Font.BOLD, 16));
		saveBtn.addActionListener(this);
		panel_12.add(saveBtn);
		
		JButton resetBtn = new JButton("重置");
		resetBtn.setIcon(new ImageIcon(ProviderAddPanel.class.getResource("/com/bct/jxc/images/reset.png")));
		resetBtn.setFont(new Font("楷体", Font.BOLD, 16));
		resetBtn.addActionListener(this);
		panel_12.add(resetBtn);
		
		JLabel label_52 = new JLabel("");
		add(label_52);
		
		JLabel label_53 = new JLabel("");
		add(label_53);
		
		JLabel label_54 = new JLabel("");
		add(label_54);
		
		JLabel label_7 = new JLabel("");
		add(label_7);
		
		JLabel label_8 = new JLabel("");
		add(label_8);
		
		JLabel label_55 = new JLabel("");
		add(label_55);
		
		JLabel label_56 = new JLabel("");
		add(label_56);
		
		JLabel label_57 = new JLabel("");
		add(label_57);
		// 初始化service
		ProviderServer = CommonFactory.getProviderServer();
	}

	/**
	 * 监听点击事件
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String target = e.getActionCommand();
		switch (target) {
		case "保存":
			boolean flag = CustomerInternalFrame.dataCheck(ProviderNameTf.getText().trim(), addressTf.getText().trim(),
					shortsTf.getText().trim(), zipTf.getText().trim(), telTf.getText().trim(), faxTf.getText().trim(),
					contactsTf.getText().trim(), phoneTf.getText().trim(), emailTf.getText().trim(),
					bankTf.getText().trim(), accountTf.getText().trim());
			if (flag) {
				// 获取供应商编号
				String id = ProviderServer.getProviderId();
				// 将供应商信息添加到数据库
				Provider Provider = new Provider(id, ProviderNameTf.getText().trim(), shortsTf.getText().trim(),
						addressTf.getText().trim(), zipTf.getText().trim(), telTf.getText().trim(),
						faxTf.getText().trim(), contactsTf.getText().trim(), phoneTf.getText().trim(),
						bankTf.getText().trim(), accountTf.getText().trim(), emailTf.getText().trim(), 1);
				boolean result = ProviderServer.addProvider(Provider);
				if (result == true) {
					JOptionPane.showMessageDialog(null, "供应商信息添加成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
					setNull();
				} else {
					JOptionPane.showMessageDialog(null, "供应商信息添加失败！", "警告", JOptionPane.WARNING_MESSAGE);
				}
			}
			break;
		case "重置":
			setNull();
			break;
		default:
			break;
		}

	}

	/**
	 * 清空文本框中的值
	 */
	private void setNull() {
		ProviderNameTf.setText("");
		shortsTf.setText("");
		addressTf.setText("");
		zipTf.setText("");
		telTf.setText("");
		faxTf.setText("");
		contactsTf.setText("");
		phoneTf.setText("");
		bankTf.setText("");
		accountTf.setText("");
		emailTf.setText("");
	}
}
