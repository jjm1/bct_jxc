package com.bct.jxc.view;

/**
 * 主界面
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.bct.jxc.model.User;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.ImageIcon;

public class MainFrame extends JFrame {
	private static MainFrame mainFrame = null;
	public static String loginName = null;//保存登录后的账号
	public static User user = null;//保存登录后的数据
	private JPanel contentPane;
	private JDesktopPane desktopPane;//可以作为桌面，停靠子窗口

	/**
	 * 初始化
	 * @return
	 */
	public static MainFrame getMainFrame() {
		if(mainFrame == null) {
			mainFrame = new MainFrame();
		}
		return mainFrame;
	}
	/**
	 * 初始化视图
	 */
	public MainFrame() {
		setFont(new Font("黑体", Font.PLAIN, 12));
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/com/bct/jxc/images/logo-s.png")));
		setTitle("企业进销存管理系统");//标题
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//标题上的开关按钮
		//锁定视窗
		setResizable(false);
		//获取屏幕的尺寸信息
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width/6, screenSize.height/6, screenSize.width*2/3, screenSize.height*2/3);//设置窗体大小
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		boolean flag = false;
		//权限管理
		if(user != null) {
			if(user.getPower().equals("超级管理员") || user.getPower().equals("普通管理员")) {
				flag = true;
				JMenu menu = new JMenu("基础信息管理");
				menu.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/info.png")));
				menu.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(menu);
				
				JMenu mnNewMenu = new JMenu("进货管理");
				mnNewMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/modeling_inventory.png")));
				mnNewMenu.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu);
				
				JMenu mnNewMenu_1 = new JMenu("销售管理");
				mnNewMenu_1.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sales.png")));
				mnNewMenu_1.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_1);
				
				JMenu mnNewMenu_2 = new JMenu("查询统计");
				mnNewMenu_2.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/query.png")));
				mnNewMenu_2.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_2);
				
				JMenu mnNewMenu_3 = new JMenu("库存管理");
				mnNewMenu_3.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/store.png")));
				mnNewMenu_3.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_3);
				
				JMenuItem mntmNewMenuItem = new JMenuItem("客户信息管理");
				mntmNewMenuItem.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/customer.png")));
				mntmNewMenuItem.setFont(new Font("黑体", Font.PLAIN, 15));
				menu.add(mntmNewMenuItem);
				//给客户信息管理设置点击事件
				mntmNewMenuItem.addActionListener(MainAction.clickCustomerInfoManager());
				JMenuItem mntmNewMenuItem_1 = new JMenuItem("商品信息管理");
				//给商品信息管理设置点击事件
				mntmNewMenuItem_1.addActionListener(MainAction.clickProductInfoManager());
				mntmNewMenuItem_1.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/goods.png")));
				mntmNewMenuItem_1.setFont(new Font("黑体", Font.PLAIN, 15));
				menu.add(mntmNewMenuItem_1);
				
				JMenuItem mntmNewMenuItem_2 = new JMenuItem("供应商信息管理");
				//给供应商信息管理设置点击事件
				mntmNewMenuItem_2.addActionListener(MainAction.clickProviderInfoManager());
				mntmNewMenuItem_2.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/provider.png")));
				mntmNewMenuItem_2.setFont(new Font("黑体", Font.PLAIN, 15));
				menu.add(mntmNewMenuItem_2);
				
				JMenuItem mntmNewMenuItem_3 = new JMenuItem("进货单管理");
				//给进货管理绑定事件
				mntmNewMenuItem_3.addActionListener(MainAction.clickInportInfoManager());
				mntmNewMenuItem_3.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/purchase_order.png")));
				mntmNewMenuItem_3.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu.add(mntmNewMenuItem_3);
				
				JMenuItem mntmNewMenuItem_4 = new JMenuItem("退货管理");
				mntmNewMenuItem_4.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/purchase_return.png")));
				mntmNewMenuItem_4.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu.add(mntmNewMenuItem_4);
				
				JMenuItem mntmNewMenuItem_5 = new JMenuItem("销售单管理");
				mntmNewMenuItem_5.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells.png")));
				mntmNewMenuItem_5.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_1.add(mntmNewMenuItem_5);
				
				JMenuItem mntmNewMenuItem_6 = new JMenuItem("销售退货");
				mntmNewMenuItem_6.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_return.png")));
				mntmNewMenuItem_6.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_1.add(mntmNewMenuItem_6);
				
				
				JMenuItem CustomerSearch = new JMenuItem("客户查询");
				//给“客户查询”绑定事件
				CustomerSearch.addActionListener(MainAction.clickCustomerInfoSearch());
				CustomerSearch.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/customer_query.png")));
				CustomerSearch.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(CustomerSearch);
				
				JMenuItem mntmNewMenuItem_8 = new JMenuItem("供应商查询");
				//给供应商查询绑定事件
				mntmNewMenuItem_8.addActionListener(MainAction.clickProviderInfoSearch());
				mntmNewMenuItem_8.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/provider_query.png")));
				mntmNewMenuItem_8.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_8);
				
				JMenuItem mntmNewMenuItem_9 = new JMenuItem("商品查询");
				//给商品查询绑定事件
				mntmNewMenuItem_9.addActionListener(MainAction.clickProductInfoSearch());
				mntmNewMenuItem_9.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/goods_query.png")));
				mntmNewMenuItem_9.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_9);
				
				JMenuItem mntmNewMenuItem_10 = new JMenuItem("销售查询");
				mntmNewMenuItem_10.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_query.png")));
				mntmNewMenuItem_10.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_10);
				
				JMenuItem mntmNewMenuItem_11 = new JMenuItem("销售退货查询");
				mntmNewMenuItem_11.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_return_query.png")));
				mntmNewMenuItem_11.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_11);
				
				JMenuItem mntmNewMenuItem_12 = new JMenuItem("入库查询");
				mntmNewMenuItem_12.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_query.png")));
				mntmNewMenuItem_12.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_12);
				
				JMenuItem mntmNewMenuItem_13 = new JMenuItem("入库退货查询");
				mntmNewMenuItem_13.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_return_query.png")));
				mntmNewMenuItem_13.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_13);
				
				JMenuItem mntmNewMenuItem_14 = new JMenuItem("库存盘点");
				mntmNewMenuItem_14.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/count.png")));
				mntmNewMenuItem_14.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_3.add(mntmNewMenuItem_14);
				
				JMenuItem mntmNewMenuItem_15 = new JMenuItem("价格调整");
				mntmNewMenuItem_15.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/price_roll.png")));
				mntmNewMenuItem_15.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_3.add(mntmNewMenuItem_15);
				
			}
			if(user.getPower().equals("超级管理员") && !(user.getPower().equals("普通管理员"))) {//扩展
				JMenu mnNewMenu_4 = new JMenu("系统管理");
				mnNewMenu_4.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/setting.png")));
				mnNewMenu_4.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_4);
				JMenuItem mntmNewMenuItem_16 = new JMenuItem("操作员管理");
				mntmNewMenuItem_16.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/operator_set.png")));
				mntmNewMenuItem_16.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_4.add(mntmNewMenuItem_16);
				JMenuItem mntmNewMenuItem_18 = new JMenuItem("权限管理");
				mntmNewMenuItem_18.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/power_set.png")));
				mntmNewMenuItem_18.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_4.add(mntmNewMenuItem_18);
				JMenuItem mntmNewMenuItem_17 = new JMenuItem("更改密码");
				mntmNewMenuItem_17.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/change_pwd.png")));
				mntmNewMenuItem_17.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_4.add(mntmNewMenuItem_17);
			}
			if(user.getPower().equals("销售员")) {
				flag = true;
				JMenu mnNewMenu_1 = new JMenu("销售管理");
				mnNewMenu_1.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sales.png")));
				mnNewMenu_1.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_1);
				
				JMenu mnNewMenu_2 = new JMenu("查询统计");
				mnNewMenu_2.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/query.png")));
				mnNewMenu_2.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_2);
				JMenuItem mntmNewMenuItem_5 = new JMenuItem("销售单管理");
				mntmNewMenuItem_5.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells.png")));
				mntmNewMenuItem_5.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_1.add(mntmNewMenuItem_5);
				
				JMenuItem mntmNewMenuItem_6 = new JMenuItem("销售退货");
				mntmNewMenuItem_6.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_return.png")));
				mntmNewMenuItem_6.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_1.add(mntmNewMenuItem_6);
				JMenuItem mntmNewMenuItem_10 = new JMenuItem("销售查询");
				mntmNewMenuItem_10.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_query.png")));
				mntmNewMenuItem_10.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_10);
				
				JMenuItem mntmNewMenuItem_11 = new JMenuItem("销售退货查询");
				mntmNewMenuItem_11.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/sells_return_query.png")));
				mntmNewMenuItem_11.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_11);
			}
			if(user.getPower().equals("采购员")) {
				flag = true;
				JMenu mnNewMenu = new JMenu("进货管理");
				
				mnNewMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/modeling_inventory.png")));
				mnNewMenu.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu);
				JMenu mnNewMenu_2 = new JMenu("查询统计");
				mnNewMenu_2.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/query.png")));
				mnNewMenu_2.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_2);
				JMenuItem mntmNewMenuItem_3 = new JMenuItem("进货单管理");
				//给进货管理绑定事件
				mntmNewMenuItem_3.addActionListener(MainAction.clickInportInfoManager());
				mntmNewMenuItem_3.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/purchase_order.png")));
				mntmNewMenuItem_3.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu.add(mntmNewMenuItem_3);
				
				JMenuItem mntmNewMenuItem_4 = new JMenuItem("退货管理");
				mntmNewMenuItem_4.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/purchase_return.png")));
				mntmNewMenuItem_4.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu.add(mntmNewMenuItem_4);
				JMenuItem mntmNewMenuItem_12 = new JMenuItem("入库查询");
				mntmNewMenuItem_12.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_query.png")));
				mntmNewMenuItem_12.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_12);
				
				JMenuItem mntmNewMenuItem_13 = new JMenuItem("入库退货查询");
				mntmNewMenuItem_13.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_return_query.png")));
				mntmNewMenuItem_13.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_13);
			}
			if(user.getPower().equals("仓库员")) {
				flag = true;
				JMenu mnNewMenu_2 = new JMenu("查询统计");
				mnNewMenu_2.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/query.png")));
				mnNewMenu_2.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_2);
				JMenu mnNewMenu_3 = new JMenu("库存管理");
				mnNewMenu_3.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/store.png")));
				mnNewMenu_3.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_3);
				
				JMenuItem mntmNewMenuItem_12 = new JMenuItem("入库查询");
				mntmNewMenuItem_12.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_query.png")));
				mntmNewMenuItem_12.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_12);
				
				JMenuItem mntmNewMenuItem_13 = new JMenuItem("入库退货查询");
				mntmNewMenuItem_13.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/entry_return_query.png")));
				mntmNewMenuItem_13.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_2.add(mntmNewMenuItem_13);
				JMenuItem mntmNewMenuItem_14 = new JMenuItem("库存盘点");
				mntmNewMenuItem_14.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/count.png")));
				mntmNewMenuItem_14.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_3.add(mntmNewMenuItem_14);
			}
			if(!user.getPower().equals("超级管理员")) {
				JMenu mnNewMenu_4 = new JMenu("系统管理");
				mnNewMenu_4.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/setting.png")));
				mnNewMenu_4.setFont(new Font("黑体", Font.PLAIN, 15));
				menuBar.add(mnNewMenu_4);
				JMenuItem mntmNewMenuItem_17 = new JMenuItem("更改密码");
				mntmNewMenuItem_17.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/change_pwd.png")));
				mntmNewMenuItem_17.setFont(new Font("黑体", Font.PLAIN, 15));
				mnNewMenu_4.add(mntmNewMenuItem_17);
			}
			if(!flag) {
				JOptionPane.showMessageDialog(null, "非法用户！","警告",JOptionPane.WARNING_MESSAGE);
				System.exit(0);
			}
		}
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(new Color(204, 204, 204));
		desktopPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		desktopPane.setLayout(new BorderLayout(0,0));//设置内部布局结构
		setContentPane(desktopPane);
		//设置拖动模式
		desktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		setContentPane(desktopPane);
		
		JLabel lblNewLabel = new JLabel("欢迎使用企业进销存管理系统！");
		lblNewLabel.setIcon(new ImageIcon(MainFrame.class.getResource("/com/bct/jxc/images/logo-x.png")));
		lblNewLabel.setFont(new Font("黑体", Font.BOLD, 24));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBackground(new Color(204, 204, 204));
		lblNewLabel.setForeground(new Color(51, 51, 51));
		desktopPane.add(lblNewLabel, BorderLayout.CENTER);
	}

}
