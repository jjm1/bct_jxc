package com.bct.jxc.view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class InportInternalFrame extends JInternalFrame {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private JTextField idTf;
	private JTextField contractTf;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the frame.
	 */
	public InportInternalFrame() {
		super("进货单", true, true, true, true);
		setBounds(0, 0, 546, 430);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		JLabel lblNewLabel = new JLabel("进货单号：");
		panel_1.add(lblNewLabel);
		
		idTf = new JTextField();
		panel_1.add(idTf);
		idTf.setColumns(20);
		
		JLabel lblNewLabel_1 = new JLabel("供应商：");
		panel_1.add(lblNewLabel_1);
		
		JComboBox comboBox = new JComboBox();
		panel_1.add(comboBox);
		
		JLabel lblNewLabel_2 = new JLabel("联系人：");
		panel_1.add(lblNewLabel_2);
		
		contractTf = new JTextField();
		panel_1.add(contractTf);
		contractTf.setColumns(20);
		
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		
		JLabel lblNewLabel_3 = new JLabel("结算方式：");
		panel_2.add(lblNewLabel_3);
		
		JComboBox comboBox_1 = new JComboBox();
		panel_2.add(comboBox_1);
		
		JLabel lblNewLabel_4 = new JLabel("进货时间：");
		panel_2.add(lblNewLabel_4);
		
		textField = new JTextField();
		textField.setColumns(20);
		panel_2.add(textField);
		
		JLabel label = new JLabel("经手人：");
		panel_2.add(label);
		
		textField_1 = new JTextField();
		textField_1.setColumns(20);
		panel_2.add(textField_1);

	}

}
